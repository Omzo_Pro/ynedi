<?php
	include('connexion.php');
	$date=date('Y-m-d H:i:s');
	$date2=date('Y-m-d');

//----------------Ajouter collection ----------------------------------------

	if(isset($_POST['add_collection'])){
		$erreur='';
		$nom=$_POST['nom'];
		$description=$_POST['description'];
		$secret=rand(0,751454);
		$req_select=$bdd->prepare('SELECT * FROM collection WHERE collection=:collection ');
		$req_select->execute(array('collection'=>$nom));
		$n=$req_select->rowCount();

		if ($n==0) {
			$req_insert=$bdd->prepare('INSERT INTO collection (idCollection, collection, description,secret) VALUES (:ID, :collection, :des, :secret)');
			$req_insert->execute(array(
									'ID'=>NULL,
									'collection'=>$nom,
									'des'=>$description,
									'secret'=>$secret
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Collection ajoutée avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec de l\'ajout. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
		}
		else{
			$erreur='
				<div class="alert alert-danger alert-dismissible fade show">
					<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
					<strong>Oups!</strong> Cette collection existe déja !!!
					<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                    </button>
				</div>';
		}

	}
//----------------Update collection ----------------------------------------

	if(isset($_POST['edit_collection'])){
		$erreur='';
		if(isset($_POST['nom'])){
			$nom=$_POST['nom'];
		}else{
			$nom='';
		}
		if(isset($_POST['description'])){
			$description=$_POST['description'];
		}else{
			$description='';
		}
		
		$secret=$_POST['secret'];
		
			$req_insert=$bdd->prepare('UPDATE collection SET collection=:nom, description=:dscr WHERE secret=:secret');
			$req_insert->execute(array(
									'nom'=>$nom,
									'dscr'=>$description,
									'secret'=>$secret
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Collection modifée avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
					<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
					<strong>Oups!</strong> veuillez réessayer !!!
					<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                    </button>
				</div>';
			}
	
	}

//---------------------Delete Collection
	if(isset($_POST['sup_col']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM collection WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Tailleur supprimé avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
	}


//----------------Ajouter Tenue ----------------------------------------

	if(isset($_POST['add_tenue'])){
		$erreur='';
		$conceptstore=$_POST['conceptstore'];
		$modele=$_POST['modele'];
		$qte=$_POST['qte'];
		$dateR=$_POST['dateR'];
		$taille=$_POST['taille'];
		$prix=$_POST['prix'];
		$description=$_POST['description'];
		$secret=rand(0,751454);

		
			$req_insert=$bdd->prepare('INSERT INTO tenues (id, conceptstore, modele, quantite, taille, prix, description, dateRegister, matricule) VALUES (:ID, :concept, :mod, :qte, :tay, :prix, :des, :dat, :secret)');
			$req_insert->execute(array(
									'ID'=>NULL,
									'concept'=>$conceptstore,
									'mod'=>$modele,
									'qte'=>$qte,
									'tay'=>$taille,
									'prix'=>$prix,
									'des'=>$description,
									'dat'=>$dateR,
									'secret'=>$secret
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Tenue ajoutée avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec de l\'ajout. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
		

	}	

//----------------Ajouter conceptstore ----------------------------------------

	if(isset($_POST['add_conceptstore'])){
		$secret=rand(0,9999999);

		
		$erreur='';
		$nom=$_POST['nom'];
		$adresse=addslashes($_POST['adresse']);
		$contact=addslashes($_POST['contact']);
		
		$logo=addslashes($_FILES['logo']['name']);
		$size=$_FILES['logo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($logo,'.'),1));
		
		if(!empty($logo)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image, $extension_valides)){
					$dossier= 'images/conceptstore/';
					$upload_image=$_FILES['logo']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$logo);
						
				}
			}
		}
		else
		{
			$logo='default.jpg';
		}
	
		$req_insert=$bdd->prepare('INSERT INTO conceptstores (id, nom, adresse, contact, logo, matricule) VALUES (:ID,:nm, :adrs, :ctc, :img, :secret)');
		$req_insert->execute(array(
								'ID'=>NULL,
								'nm'=>$nom,
								'adrs'=>$adresse,
								'ctc'=>$contact,
								'img'=>$logo,
								'secret'=>$secret
							));
		
		
		if($req_insert){
			$erreur='
				<div class="alert alert-success alert-dismissible fade show">
					<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
					<strong>Success!</strong> ConceptStore ajouté avec sucés.
					<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                    </button>
				</div>';
		}
		else{
			$erreur='
				<div class="alert alert-danger alert-dismissible fade show">
					<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
					<strong>Oups!</strong>  Echec de l\'ajout. Veuillez réessayer !!!
					<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                    </button>
				</div>';
		}
	}

//----------------Update Conceptstore ---------------------------
	if(isset($_POST['edit_conceptstore'])){
		$erreur='';




		if(isset($_POST['nom'])){
			$nom=$_POST['nom'];
		}else{
			$nom='';
		}

		if(isset($_POST['adresse'])){
			$adresse=$_POST['adresse'];
		}else{
			$adresse='';
		}
		if(isset($_POST['contact'])){
			$contact=$_POST['contact'];
		}else{
			$contact='';
		}
		$matricule=$_POST['matricule'];

		$image1=$_POST['logo'];
		
		$logo=$_FILES['logo']['name'];
		$size=$_FILES['logo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($logo,'.'),1));

		if(!empty($logo)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image, $extension_valides)){
					$dossier= 'images/conceptstore/';
					$upload_image=$_FILES['logo']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$logo);
						
				}
			}
		}
		else
		{
			$logo=$image1;
		}
			$req_update_art=$bdd->prepare('UPDATE conceptstores SET nom=:nm, logo=:img, adresse=:adresse, contact=:contact WHERE matricule=:ref');
			$req_update_art->execute(array(
									'nm'=>$nom,
									'img'=>$logo,
									'adresse'=>$adresse,
									'contact'=>$contact,
									'ref'=>$matricule));
			
			
			if($req_update_art){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> ConceptStore modifié avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec de la modification. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
				
		/*}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Aucun article trouvé.
				</div>';
		}*/
	}

//---------------------Delete Commande
	if(isset($_POST['sup_concept']))
	{
			
		$erreur='';
		$matricule=$_POST['matricule'];
													
		$req_supp=$bdd->prepare('DELETE FROM conceptstores WHERE matricule=:mat');
		$req_supp->execute(array('mat'=>$matricule));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> ConceptStore supprimé avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
	}

//----------------Ajouter une modele ----------------------------------------

	if(isset($_POST['add_modele'])){
		$secret=rand(0,9999999);

		
		$erreur='';
		$titre=$_POST['titre'];
		$prix=$_POST['prix'];
		$collection=$_POST['collection'];
		$desc=addslashes($_POST['description']);
		
		$photo=addslashes($_FILES['photo']['name']);
		$size=$_FILES['photo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($photo,'.'),1));
		
		if(!empty($photo)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image, $extension_valides)){
					$dossier= 'images/modeles/';
					$upload_image=$_FILES['photo']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$photo);
						
				}
			}
		}
		else
		{
			$photo='default.jpg';
		}
	
		$req_insert=$bdd->prepare('INSERT INTO modeles (idModele, titre, collection, description, prix, photo, reference, date_ajout) VALUES (:ID,:titre, :col, :description,:prix,:img,:secret,:dat)');
		$req_insert->execute(array(
								'ID'=>NULL,
								'titre'=>$titre,
								'col'=>$collection,
								'description'=>$desc,
								'prix'=>$prix,
								'img'=>$photo,
								'secret'=>$secret,
								'dat'=>$date
							));
		
		
		if($req_insert){
			$erreur='
				<div class="alert alert-success alert-dismissible fade show">
					<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
					<strong>Success!</strong> Modèle ajoutée avec sucés.
					<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                    </button>
				</div>';
		}
		else{
			$erreur='
				<div class="alert alert-danger alert-dismissible fade show">
					<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
					<strong>Oups!</strong>  Echec de l\'ajout. Veuillez réessayer !!!
					<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                    </button>
				</div>';
		}
	}

//----------------Update modele ---------------------------
	if(isset($_POST['edit_modele'])){
		$erreur='';
		if(isset($_POST['titre'])){
			$titre=$_POST['titre'];
		}else{
			$titre='';
		}

		if(isset($_POST['prix'])){
			$prix=$_POST['prix'];
		}else{
			$prix='';
		}
		if(isset($_POST['description'])){
			$description=$_POST['description'];
		}else{
			$description='';
		}
		$reference=$_POST['reference'];

		$image1=$_POST['photo'];
		
		$photo=$_FILES['photo']['name'];
		$size=$_FILES['photo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($photo,'.'),1));

		if(!empty($photo)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image, $extension_valides)){
					$dossier= 'images/modeles/';
					$upload_image=$_FILES['photo']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$photo);
						
				}
			}
		}
		else
		{
			$photo=$image1;
		}
			$req_update_art=$bdd->prepare('UPDATE modeles SET titre=:titre, photo=:img, description=:description, prix=:prix WHERE reference=:ref');
			$req_update_art->execute(array(
									'titre'=>$titre,
									'img'=>$photo,
									'description'=>$description,
									'prix'=>$prix,
									'ref'=>$reference));
			
			
			if($req_update_art){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Modèle modifiée avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec de la modification. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
				
		/*}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Aucun article trouvé.
				</div>';
		}*/
	}

//---------------------Delete Commande
	if(isset($_POST['sup_modele']))
	{
			
		$erreur='';
		$reference=$_POST['reference'];
													
		$req_supp=$bdd->prepare('DELETE FROM modeles WHERE reference=:ref');
		$req_supp->execute(array('ref'=>$reference));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Modèle supprimée avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
	}
//----------- ajouter un tailleur

	if(isset($_POST['add_tailleur'])){
		$erreur='';
		$email=$_POST['email'];
		$nom=$_POST['nom'];
		$prenom=$_POST['prenom'];
		$adresse=$_POST['adresse'];
		$telephone=$_POST['telephone'];
		$role='tailleur';

		$secret=rand(0,9999999);
		$profile="default.png";
		
		$recto=addslashes($_FILES['recto']['name']);
		$size=$_FILES['recto']['size'];
		$extension_valide1= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image1= strtolower(substr(strrchr($recto,'.'),1));

		if(!empty($recto)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image1, $extension_valide1)){
					$dossier= 'images/tailleur/';
					$upload_image=$_FILES['recto']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$recto);
						
				}
			}
		}
		else
		{
			$recto='default.jpg';
		}

		$verso=addslashes($_FILES['verso']['name']);
		$size=$_FILES['verso']['size'];
		$extension_valide2= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image2= strtolower(substr(strrchr($verso,'.'),1));

		if(!empty($verso)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image2, $extension_valide2)){
					$dossier= 'images/tailleur/';
					$upload_image=$_FILES['verso']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$verso);
						
				}
			}
		}
		else
		{
			$verso='default.jpg';
		}
		
		$req_select=$bdd->prepare('SELECT * FROM tailleur WHERE telephone=:log');
		$req_select->execute(array('log'=>$telephone));
		$n=$req_select->rowCount();
		
		if($n==0){
			
			$req_insert=$bdd->prepare('INSERT INTO tailleur (idTailleur, profile, idrecto, idverso, prenom, nom, adresse, telephone,  email, matricule, activation) VALUES (:id, :prof, :rec, :ver, :prnm, :nm, :adrs, :tel, :mail, :mat, :act)');
			$req_insert->execute(array(
									'id'=>NULL,
									'prof'=>$profile,
									'rec'=>$recto,
									'ver'=>$verso,
									'prnm'=>$prenom,
									'nm'=>$nom,
									'adrs'=>$adresse,
									'tel'=>$telephone,
									'mail'=>$email,
									'mat'=>$secret,
									'act'=>1
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Tailleur ajouté avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec de l\'ajout. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}				
		}
		else{
			$erreur='
				<div class="alert alert-danger alert-dismissible fade show">
					<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
					<strong>Erreur!</strong>Ce numéro de telephone est déjà associé à un compte !!!
					<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                    </button>
				</div>';
		}
	}

//---------------------Delete Collection
	if(isset($_POST['sup_tail']))
	{
			
		$erreur='';
		$matricule=$_POST['matricule'];
													
		$req_supp=$bdd->prepare('DELETE FROM tailleur WHERE matricule=:mat');
		$req_supp->execute(array('mat'=>$matricule));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Collection supprimée avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
	}

//----------------Ajouter une cliente ----------------------------------------

	if(isset($_POST['addclient'])){

		$erreur='';

		//client
		$prenom=$_POST['prenom'];
		$nom=$_POST['nom'];
		$naissance=$_POST['naissance'];
		$month=substr($naissance, 5, 2);
		$day=substr($naissance, 8, 2);

		$commentaire=$_POST['commentaire'];
		$whatsapp=$_POST['whatsapp'];
		$adresse=$_POST['adresse'];
		$email=$_POST['email'];
		$telephone=$_POST['telephone'];

		$mat1 = substr(strtoupper($nom),0,2);
		$mat2 = substr(strtoupper($prenom),0,2);
		$mat3 = substr($telephone,5,3);
		$secret2=rand(0,123);

		$matriculeClient=$mat1.$mat3.$mat2.$day.$month;
		//client

		//mesure
		$epaule=$_POST['epaule'];
		$poitrine=$_POST['poitrine'];
		$taille=$_POST['taille'];
		$ceinture=$_POST['ceinture'];
		$hanche=$_POST['hanche'];
		$blouse=$_POST['blouse'];
		$cou=$_POST['cou'];
		$tmanche=$_POST['tmanche'];
		$poignet=$_POST['poignet'];
		$cuisses=$_POST['cuisses'];
		$manche34=$_POST['manche34'];
		$mancheLongue=$_POST['mancheLongue'];
		$ljupe=$_POST['ljupe'];
		$lpantalon=$_POST['lpantalon'];
		$lrobe=$_POST['lrobe'];
		$lboubou=$_POST['lboubou'];

		$mancheCourte=$_POST['mancheCourte'];
		$jupe34=$_POST['jupe34'];
		$longRobe34=$_POST['longRobe34'];
		//mesure

	
		$req_select_cli=$bdd->prepare('SELECT * FROM clients WHERE matricule=:mat');
		$req_select_cli->execute(array('mat'=>$matriculeClient));
		$n=$req_select_cli->rowCount();

		if($n==0){
		$req_insert_cli=$bdd->prepare('INSERT INTO clients (idClient, prenom, nom, adresse, email, telephone, commentaire, whatsapp, jourNaiss, moisNaiss, matricule, dateRegister) VALUES (:ID,:prnm, :nm, :adrs, :mail, :tel, :com, :ref, :jnes, :mnes, :secret, :dat)');
		$req_insert_cli->execute(array(
			'ID'=>NULL,
			'prnm'=>$prenom,
			'nm'=>$nom,
			'adrs'=>$adresse,
			'mail'=>$email,
			'tel'=>$telephone,
			'com'=>$commentaire,
			'ref'=>$whatsapp,
			'jnes'=>$day,
			'mnes'=>$month,
			'secret'=>$matriculeClient,
			'dat'=>$date
			));
			if($req_insert_cli){
				$req_insert_mesure=$bdd->prepare('INSERT INTO mesures (idMesure, epaule, poitrine, taille, ceinture, hanche, blouse, cou, tManches, poignet, cuisses, manche34, mancheLongue, longJupe, longPantalon, longRobe, longBoubou, jupe34, longRobe34, mancheCourte, client) VALUES (:ID,:epol, :poit, :tay, :ctr, :hche, :blz, :cou, :tmche,:pgnt, :c8s, :m34, :manlong, :ljup, :lptln, :lrob, :lbubu, :jup34, :lr34, :mancourt, :cli)');
				$req_insert_mesure->execute(array(
					'ID'=>NULL,
					'epol'=>$epaule,
					'poit'=>$poitrine,
					'tay'=>$taille,
					'ctr'=>$ceinture,
					'hche'=>$hanche,
					'blz'=>$blouse,
					'cou'=>$cou,
					'tmche'=>$tmanche,
					'pgnt'=>$poignet,
					'c8s'=>$cuisses,
					'm34'=>$manche34,
					'manlong'=>$mancheLongue,
					'ljup'=>$ljupe,
					'lptln'=>$lpantalon,
					'lrob'=>$lrobe,
					'lbubu'=>$lboubou,
					'jup34'=>$jupe34,
					'lr34'=>$longRobe34,
					'mancourt'=>$mancheCourte,
					'cli'=>$matriculeClient));
					if($req_insert_mesure){
						$erreur='
						<div class="alert alert-success alert-dismissible fade show">
								<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
								<strong>Success!</strong>Cliente enregistrée avec sucés.
								<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
			                    </button>
							</div>';
						
							
					}else{
						$erreur='
						<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec de l\'enregistrement de la mesure!!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
						</button>
						</div>';
					}
			}else{
				$erreur='
				<div class="alert alert-danger alert-dismissible fade show">
				<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
				<strong>Oups!</strong>  Echec de l\'enregistrement de la cliente!!!
				<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
				</button>
				</div>';
			}
		}else{
			$erreur='
				<div class="alert alert-danger alert-dismissible fade show">
				<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
				<strong>Oups!</strong> Cliente déja enregistrée!!!
				<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
				</button>
				</div>';
		}

	}
//----------------Ajouter une commande/retouche pour une cliente existante----------------------------------------

	if(isset($_POST['add_commande1'])){
		
		$erreur='';

		//client
		$client=$_POST['client'];
		$prix=0;
		$note=$_POST['note'];
		$modele=json_encode($_POST['modele']);
		$modele2 = json_decode($modele);

	
		foreach ($modele2 as $mod) {
			$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
            $req_modele->execute(array('ref'=>$mod));
            $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
            $prix1=$donnees_mod['prix'];
            $prix+=$prix1;
		}
									



		$type=$_POST['type'];
		$avance=$_POST['avance'];
		$remise=$_POST['remise'];
		$reference=rand(0,9999999);
		$etat="enregistrée";
		
		$reste=$prix-$avance;
		if ($reste==0) {
			$paiement='payé';
		}else{
			$paiement='accompte';
		}

		$delai=$_POST['delai'];
		$dateLivraison= date('Y-m-d', strtotime($date2. ' + '.$delai.' days'));


		$req_insert_commande=$bdd->prepare('INSERT INTO commandes (idCommande, type, modele, tailleur, client, dateCommande, delaiLivraison, dateLivraison, etat, note, paiement, somme, avance, reste, remise, reference) VALUES (:ID, :tip, :mod, :tail, :cli, :dat, :dele, :datL, :eta, :nt, :pai, :som, :ava, :rest, :rem, :ref)');
		$req_insert_commande->execute(array(
								'ID'=>NULL,
								'tip'=>$type,
								'mod'=>$modele,
								'tail'=>0,
								'cli'=>$client,
								'dat'=>$date2,
								'dele'=>$delai,
								'datL'=>$dateLivraison,
								'eta'=>$etat,
								'nt'=>$note,
								'pai'=>$paiement,
								'som'=>$prix,
								'ava'=>$avance,
								'rest'=>$reste,
								'rem'=>$remise,
								'ref'=>$reference
							));
					if($req_insert_commande){
						$erreur='
							<div class="alert alert-success alert-dismissible fade show">
								<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
								<strong>Success!</strong> '.$type.' enregistrée avec sucés.
								<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
			                    </button>
							</div>';
					}
					else{
						$erreur='
							<div class="alert alert-danger alert-dismissible fade show">
								<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
								<strong>Oups!</strong>  Echec de l\'enregistrement. Veuillez réessayer !!!
								<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
			                    </button>
							</div>';
					}
	}

//----------------Ajouter une commande/retouche ----------------------------------------

	if(isset($_POST['add_commande'])){

		$erreur='';

		//client
		$prenom=$_POST['prenom'];
		$nom=$_POST['nom'];
		$naissance=$_POST['naissance'];
		$month=substr($naissance, 5, 2);
		$day=substr($naissance, 8, 2);

		$commentaire=$_POST['commentaire'];
		$whatsapp=$_POST['whatsapp'];
		$adresse=$_POST['adresse'];
		$email=$_POST['email'];
		$telephone=$_POST['telephone'];

		$mat1 = substr(strtoupper($nom),0,2);
		$mat2 = substr(strtoupper($prenom),0,2);
		$mat3 = substr($telephone,5,3);
		$secret2=rand(0,123);

		$matriculeClient=$mat1.$mat3.$mat2.$day.$month;
		//client

		//mesure
		$epaule=$_POST['epaule'];
		$poitrine=$_POST['poitrine'];
		$taille=$_POST['taille'];
		$ceinture=$_POST['ceinture'];
		$hanche=$_POST['hanche'];
		$blouse=$_POST['blouse'];
		$manches=$_POST['manches'];
		$cou=$_POST['cou'];
		$tmanche=$_POST['tmanche'];
		$poignet=$_POST['poignet'];
		$cuisses=$_POST['cuisses'];
		$manche34=$_POST['manche34'];
		$mancheLongue=$_POST['mancheLongue'];
		$ljupe=$_POST['ljupe'];
		$lpantalon=$_POST['lpantalon'];
		$lrobe=$_POST['lrobe'];
		$lboubou=$_POST['lboubou'];

		$mancheCourte=$_POST['mancheCourte'];
		$jupe34=$_POST['jupe34'];
		$longRobe34=$_POST['longRobe34'];
		//mesure

		//ajout
		$prix=0;
		$modele=json_encode($_POST['modele']);
		$modele2 = json_decode($modele);

	
		foreach ($modele2 as $mod) {
			$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
            $req_modele->execute(array('ref'=>$mod));
            $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
            $prix1=$donnees_mod['prix'];
            $prix+=$prix1;
		}
								
		$avance=$_POST['avance'];
		$remise=$_POST['remise'];
		
		$reste=$prix-$avance;
		if ($reste==0) {
			$paiement='payé';
		}else{
			$paiement='accompte';
		}
		//ajout

		$modele=json_encode($_POST['modele']);
		$note=$_POST['note'];
		$type=$_POST['type'];
		$reference=rand(0,9999999);
		$etat="enregistrée";
		$delai=$_POST['delai'];
		$dateLivraison= date('Y-m-d', strtotime($date2. ' + '.$delai.' days'));


		$req_select_cli=$bdd->prepare('SELECT * FROM clients WHERE matricule=:mat');
		$req_select_cli->execute(array('mat'=>$matriculeClient));
		$n=$req_select_cli->rowCount();

		if($n==0){
		$req_insert_cli=$bdd->prepare('INSERT INTO clients (idClient, prenom, nom, adresse, email, telephone, commentaire, whatsapp, jourNaiss, moisNaiss, matricule, dateRegister) VALUES (:ID,:prnm, :nm, :adrs, :mail, :tel, :com, :ref, :jnes, :mnes, :secret, :dat)');
		$req_insert_cli->execute(array(
			'ID'=>NULL,
			'prnm'=>$prenom,
			'nm'=>$nom,
			'adrs'=>$adresse,
			'mail'=>$email,
			'tel'=>$telephone,
			'com'=>$commentaire,
			'ref'=>$whatsapp,
			'jnes'=>$day,
			'mnes'=>$month,
			'secret'=>$matriculeClient,
			'dat'=>$date
			));
			if($req_insert_cli){
				$req_insert_mesure=$bdd->prepare('INSERT INTO mesures (idMesure, epaule, poitrine, taille, ceinture, hanche, blouse, manches, cou, tManches, poignet, cuisses, manche34, mancheLongue, longJupe, longPantalon, longRobe, longBoubou, jupe34, longRobe34, mancheCourte, client) VALUES (:ID,:epol, :poit, :tay, :ctr, :hche, :blz, :mche, :cou, :tmche,:pgnt, :c8s, :m34, :manlong, :ljup, :lptln, :lrob, :lbubu, :jup34, :lr34, :mancourt, :cli)');
				$req_insert_mesure->execute(array(
					'ID'=>NULL,
					'epol'=>$epaule,
					'poit'=>$poitrine,
					'tay'=>$taille,
					'ctr'=>$ceinture,
					'hche'=>$hanche,
					'blz'=>$blouse,
					'mche'=>$manches,
					'cou'=>$cou,
					'tmche'=>$tmanche,
					'pgnt'=>$poignet,
					'c8s'=>$cuisses,
					'm34'=>$manche34,
					'manlong'=>$mancheLongue,
					'ljup'=>$ljupe,
					'lptln'=>$lpantalon,
					'lrob'=>$lrobe,
					'lbubu'=>$lboubou,
					'jup34'=>$jupe34,
					'lr34'=>$longRobe34,
					'mancourt'=>$mancheCourte,
					'cli'=>$matriculeClient));
					if($req_insert_mesure){
						$req_insert_commande=$bdd->prepare('INSERT INTO commandes (idCommande, type, modele, tailleur, client, dateCommande, delaiLivraison, dateLivraison, etat, note, paiement, somme, avance, reste, remise, reference) VALUES (:ID, :tip, :mod, :tail, :cli, :dat, :dele, :datL, :eta, :nt, :pai, :som, :ava, :rest, :rem, :ref)');
						$req_insert_commande->execute(array(
								'ID'=>NULL,
								'tip'=>$type,
								'mod'=>$modele,
								'tail'=>0,
								'cli'=>$matriculeClient,
								'dat'=>$date2,
								'dele'=>$delai,
								'datL'=>$dateLivraison,
								'eta'=>$etat,
								'nt'=>$note,
								'pai'=>$paiement,
								'som'=>$prix,
								'ava'=>$avance,
								'rest'=>$reste,
								'rem'=>$remise,
								'ref'=>$reference
							));
							if($req_insert_commande){
								$erreur='
								<div class="alert alert-success alert-dismissible fade show">
									<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
									<strong>Success!</strong> '.$type.'  enregistrée avec sucés.
									<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
									</button>
								</div>';
							}else{
								$erreur='
								<div class="alert alert-danger alert-dismissible fade show">
								<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
								<strong>Oups!</strong>  Echec de l\'enregistrement. Veuillez réessayer !!!
								<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
								</button>
								</div>';
							}
					}else{
						$erreur='
						<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec de l\'enregistrement de la cliente!!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
						</button>
						</div>';
					}
			}else{
				$erreur='
				<div class="alert alert-danger alert-dismissible fade show">
				<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
				<strong>Oups!</strong>  Echec de l\'enregistrement de la mesure!!!
				<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
				</button>
				</div>';
			}
		}else{
			$erreur='
				<div class="alert alert-danger alert-dismissible fade show">
				<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
				<strong>Oups!</strong> Cliente déja enregistrée!!!
				<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
				</button>
				</div>';
		}

	}

//----------------Ajouter une commande/retouche pour une cliente existante----------------------------------------

	if(isset($_POST['edit_commande'])){
		
		$erreur='';

		//client
		$tailleur=$_POST['tailleur'];
		$prix=0;
		$note=$_POST['note'];
		$reference=$_POST['reference'];
		$modele=json_encode($_POST['modele']);
		$modele2 = json_decode($modele);

	
		foreach ($modele2 as $mod) {
			$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
            $req_modele->execute(array('ref'=>$mod));
            $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
            $prix1=$donnees_mod['prix'];
            $prix+=$prix1;
		}
									

		$avance=$_POST['avance'];
		$remise=$_POST['remise'];
		
		$reste=$prix-$avance;
		if ($reste==0) {
			$paiement='payé';
		}else{
			$paiement='accompte';
		}

		$delai=$_POST['delai'];
		$dateLivraison= date('Y-m-d', strtotime($date2. ' + '.$delai.' days'));


		$req_update_commande=$bdd->prepare('UPDATE commandes SET modele=:mod, tailleur=:tail, delaiLivraison=:dele, dateLivraison=:datL, note=:nt, paiement=:pai, somme=:som, avance=:ava, reste=:rest, remise=:rem WHERE reference=:ref');
		$req_update_commande->execute(array(
								'mod'=>$modele,
								'tail'=>$tailleur,
								'dele'=>$delai,
								'datL'=>$dateLivraison,
								'nt'=>$note,
								'pai'=>$paiement,
								'som'=>$prix,
								'ava'=>$avance,
								'rest'=>$reste,
								'rem'=>$remise,
								'ref'=>$reference
							));
					if($req_update_commande){
						$erreur='
							<div class="alert alert-success alert-dismissible fade show">
								<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
								<strong>Success!</strong> Modification effectuée avec sucés.
								<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
			                    </button>
							</div>';
					}
					else{
						$erreur='
							<div class="alert alert-danger alert-dismissible fade show">
								<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
								<strong>Oups!</strong>  Echec. Veuillez réessayer !!!
								<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
			                    </button>
							</div>';
					}
	}

//---------------------Delete Commande
	if(isset($_POST['sup_com']))
	{
			
		$erreur='';
		$reference=$_POST['reference'];
													
		$req_supp=$bdd->prepare('DELETE FROM commandes WHERE reference=:ref');
		$req_supp->execute(array('ref'=>$reference));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong>Commande supprimée avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
	}
//----------------Assigner Tailleur ----------------------------------------

	if(isset($_POST['assign'])){
		$erreur='';
		$reference=$_POST['reference'];
		$tailleur=$_POST['tailor'];
	
			$req_insert=$bdd->prepare('UPDATE commandes SET tailleur=:tail WHERE reference=:ref');
			$req_insert->execute(array(
									'tail'=>$tailleur,
									'ref'=>$reference
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Commande assignée avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong> Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
	
	}

//----------------Changement d'etat des commandes----------------------------

	if(isset($_POST['changement_etat'])){
		
		$reference=$_POST['reference'];
		$etat=$_POST['etat'];
		$dat1=$_POST['dat1'];
		$dat2=$_POST['dat2'];
		$datLiv=$_POST['datLiv'];
		if ($etat=="en Conception") {
			$dat1=$date2;
		}elseif ($etat=="terminée") {
			$dat2=$date2;
		}elseif ($etat=="livrée") {
			$datLiv=$date2;
		}
			$req_insert=$bdd->prepare('UPDATE commandes SET etat=:etat, dateConception=:dt1, dateLivraison=:datLiv, dateEnd=:dt2 WHERE reference=:secret');
			$req_insert->execute(array(
									'etat'=>$etat,
									'dt1'=>$dat1,
									'datLiv'=>$datLiv,
									'dt2'=>$dat2,
									'secret'=>$reference
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Etat modifié avec sucés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec de la modification. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
	
	}

//------------------- Update Client ---------------------------
	if(isset($_POST['updateclient'])){

		$erreur='';
		$matricule=$_POST['matricule'];
		//client
		$prenom=$_POST['prenom'];
		$nom=$_POST['nom'];
		$jourNaiss=$_POST['jourNaiss'];
		$moisNaiss=$_POST['moisNaiss'];
		$commentaire=$_POST['commentaire'];
		$whatsapp=$_POST['whatsapp'];
		$adresse=$_POST['adresse'];
		$email=$_POST['email'];
		$telephone=$_POST['telephone'];

		
		//client

		//mesure
		$epaule=$_POST['epaule'];
		$poitrine=$_POST['poitrine'];
		$taille=$_POST['taille'];
		$ceinture=$_POST['ceinture'];
		$hanche=$_POST['hanche'];
		$blouse=$_POST['blouse'];
		$cou=$_POST['cou'];
		$tmanche=$_POST['tmanche'];
		$poignet=$_POST['poignet'];
		$cuisses=$_POST['cuisses'];
		$manche34=$_POST['manche34'];
		$mancheLongue=$_POST['mancheLongue'];
		$ljupe=$_POST['ljupe'];
		$lpantalon=$_POST['lpantalon'];
		$lrobe=$_POST['lrobe'];
		$lboubou=$_POST['lboubou'];
		$mancheCourte=$_POST['mancheCourte'];
		$jupe34=$_POST['jupe34'];
		$longRobe34=$_POST['longRobe34'];
		//mesure

		
		$req_select=$bdd->prepare('SELECT * FROM clients WHERE matricule=:ref');
		$req_select->execute(array('ref'=>$matricule));
		$donnees=$req_select->fetch(PDO::FETCH_ASSOC);
		$n=$req_select->rowCount();
		
		if($n>0){
			$req_update_cli=$bdd->prepare('UPDATE clients SET prenom=:prnm, nom=:nm, adresse=:adrs, email=:mail, telephone=:tel, commentaire=:com, whatsapp=:wtsp, jourNaiss=:jnes, moisNaiss=:mnes WHERE matricule=:secret');
			$req_update_cli->execute(array(
									'prnm'=>$prenom,
									'nm'=>$nom,
									'adrs'=>$adresse,
									'mail'=>$email,
									'tel'=>$telephone,
									'com'=>$commentaire,
									'wtsp'=>$whatsapp,
									'jnes'=>$jourNaiss,
									'mnes'=>$moisNaiss,
									'secret'=>$matricule
								));

			$req_update_mes=$bdd->prepare('UPDATE mesures SET epaule=:epol, poitrine=:poit, taille=:tay, ceinture=:ctr, hanche=:hche, blouse=:blz, cou=:cou, tManches=:tmche, poignet=:pgnt, cuisses=:c8s, manche34=:m34, mancheLongue=:manlong, longJupe=:ljup, longPantalon=:lptln, longRobe=:lrob, longBoubou=:lbubu, jupe34=:jup34, longRobe34=:lr34, mancheCourte=:mancourt WHERE client=:cli');
			$req_update_mes->execute(array(
									'epol'=>$epaule,
									'poit'=>$poitrine,
									'tay'=>$taille,
									'ctr'=>$ceinture,
									'hche'=>$hanche,
									'blz'=>$blouse,
									'cou'=>$cou,
									'tmche'=>$tmanche,
									'pgnt'=>$poignet,
									'c8s'=>$cuisses,
									'm34'=>$manche34,
									'manlong'=>$mancheLongue,
									'ljup'=>$ljupe,
									'lptln'=>$lpantalon,
									'lrob'=>$lrobe,
									'lbubu'=>$lboubou,
									'jup34'=>$jupe34,
									'lr34'=>$longRobe34,
									'mancourt'=>$mancheCourte,
									'cli'=>$matricule));
		
			if (($req_update_cli) AND ($req_update_mes)){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Mise à jour de la cliente réussie.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
						</button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong> veuillez réessayer!!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
						</button>
					</div>';
			}
				
		}
		else{
			$erreur='
				<div class="alert alert-danger alert-dismissible fade show">
					<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
					<strong>Oups!</strong> Aucune cliente trouvée!!!
					<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
					</button>
				</div>';
		}
	}

//---------------------Delete client
	if(isset($_POST['sup_cli']))
	{
			
		$erreur='';
		$matricule=$_POST['matricule'];
													
		$req_supp=$bdd->prepare('DELETE FROM clients WHERE matricule=:mat');
		$req_supp->execute(array('mat'=>$matricule));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Cliente supprimée avec succés.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
						</button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
	}

//----------------Verifier Cliente----------------------------

	if(isset($_POST['verifier'])){
		
		$matricule=$_POST['matricule'];
			$req_verifier=$bdd->prepare('UPDATE clients SET verification=1 WHERE matricule=:mat');
			$req_verifier->execute(array(
									'mat'=>$matricule
								));
			
			
			if($req_verifier){
				$erreur='
					<div class="alert alert-success alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
						<strong>Success!</strong> Cliente verifiée.
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-danger alert-dismissible fade show">
						<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
						<strong>Oups!</strong>  Echec. Veuillez réessayer !!!
						<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	                    </button>
					</div>';
			}
	
	}

//----------------Modifier Vente ----------------------------------------

	if(isset($_POST['edit_vente'])){
		$erreur='';
		if(!empty($_POST['qteprod'])){
			$quantity=$_POST['qteprod'];
		}else{
			$quantity=1;
		}
		$secret=$_POST['secret'];
		$montant=$_POST['montant'];
		$quantite=$_POST['qteprod'];
		
		$req_insert=$bdd->prepare('UPDATE vente SET quantity=:qte, montant=:mtn WHERE matricule_vente=:secret');
		$req_insert->execute(array(
								'qte'=>$quantite,
								'mtn'=>$montant,
								'secret'=>$secret
							));
		
		
		if($req_insert){
			$erreur='
				<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-check-all"></i>
					<strong>Mise à jour réussie!</strong>
				</div>';
				header('location:?p=list_vente');
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Oups!</strong> Echec de mise à jour . Veuillez réessayer !!!
				</div>';
		}
	} 
//----------------delete produit vente ----------------------------------------

	if(isset($_POST['delete_prod_vente'])){
		$erreur='';

		$secret=$_POST['matricule'];
		$idProduit=$_POST['idProd'];

		$req_selectpr=$bdd->prepare('SELECT * FROM produit WHERE ID=:ID');
		$req_selectpr->execute(array('ID'=>$idProduit));
		$donneesprd=$req_selectpr->fetch(PDO::FETCH_ASSOC);
		$stock=$donneesprd['stock'];

		$req_selectprvdu=$bdd->prepare('SELECT * FROM produitsvendus WHERE matricule_vente=:secret AND id_article=:id');
		$req_selectprvdu->execute(array('secret'=>$secret,'id'=>$idProduit));
		$donneesprdvdu=$req_selectprvdu->fetch(PDO::FETCH_ASSOC);
		$quantity=$donneesprdvdu['quantity'];

		$newStock=$stock+$quantity;
		
		$req_insert=$bdd->prepare('DELETE FROM produitsvendus  WHERE matricule_vente=:secret AND id_article=:id');
		$req_insert->execute(array(
								'secret'=>$secret,
								'id'=>$idProduit
							));
		$req_update=$bdd->prepare('UPDATE produit SET stock=:stk WHERE ID=:id');
		$req_update->execute(array(
									'stk'=>$newStock,
									'id'=>$idProduit
								));

		
		
		if(($req_insert) AND ($req_update)){
			$erreur='
				<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-check-all"></i>
					<strong>Suppréssion réussie!</strong>
				</div>';
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Oups!</strong> Echec de la suppréssion. Veuillez réessayer !!!
				</div>';
		}
	} 
//----------------delete vente ----------------------------------------

if(isset($_POST['delete_vente'])){
	$erreur='';

	$secret=$_POST['secret'];
	$quantity=$_POST['qtevente'];
	if ($quantity==0){
		$req_del_vente=$bdd->prepare('DELETE FROM vente WHERE matricule_vente=:secret');
		$req_del_vente->execute(array(
		'secret'=>$secret,
		));
		if($req_del_vente){
			$erreur='
			<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			<i class="mdi mdi-check-all"></i>
			<strong>Suppréssion 0 réussie!</strong>
			</div>';
		}
		else{
			$erreur='
			<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			<i class="mdi mdi-block-helper"></i>
			<strong>Oups!</strong> Echec de la suppréssion 0. Veuillez réessayer !!!
			</div>';
		}
	}else{

		$req_selectprvdu=$bdd->prepare('SELECT * FROM produitsvendus WHERE matricule_vente=:secret');
		$req_selectprvdu->execute(array('secret'=>$secret));
		while ($donneesprdvdu=$req_selectprvdu->fetch(PDO::FETCH_ASSOC)) {
			$idProd=$donneesprdvdu['id_article'];
			$quantity=$donneesprdvdu['quantity'];

			$req_selectpr=$bdd->prepare('SELECT * FROM produit WHERE ID=:ID');
			$req_selectpr->execute(array('ID'=>$idProd));
			$donneesprd=$req_selectpr->fetch(PDO::FETCH_ASSOC);
			$stock=$donneesprd['stock'];

			$newStock=$stock+$quantity;

			$req_update=$bdd->prepare('UPDATE produit SET stock=:stk WHERE ID=:id');
			$req_update->execute(array(
			'stk'=>$newStock,
			'id'=>$idProd
			));
			if ($req_update) {
				$req_del_prod_vente=$bdd->prepare('DELETE FROM produitsvendus WHERE matricule_vente=:secret');
				$req_del_prod_vente->execute(array(
				'secret'=>$secret,
				));

				$req_del_vente=$bdd->prepare('DELETE FROM vente WHERE matricule_vente=:secret');
				$req_del_vente->execute(array(
				'secret'=>$secret,
				));

				if($req_del_prod_vente)
				{
					$req_del_vente=$bdd->prepare('DELETE FROM vente WHERE matricule_vente=:secret');
					$req_del_vente->execute(array(
					'secret'=>$secret,
					));
						if ($req_del_vente) {
							$erreur='
							<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
							<i class="mdi mdi-check-all"></i>
							<strong>Suppréssion réussie!</strong>
							</div>';
						}else{
							$erreur='
							<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
							<i class="mdi mdi-block-helper"></i>
							<strong>Oups!</strong> echec de la supression !!!
							</div>';

						}
				}
				else{
					$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Oups!</strong> les produits ne sont pas supprimés de la vente !!!
					</div>';
				}
			}else{
				$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<i class="mdi mdi-block-helper"></i>
				<strong>Oups!</strong> Echec de la mise a jour du stock. Veuillez réessayer !!!
				</div>';
			}

		}
	}


}
//----------- ajouter un gerant

	if(isset($_POST['add_geran'])){
		$erreur='';
		$boutique=$_POST['boutique'];
		$email=$_POST['email'];
		$nom=$_POST['nom'];
		$prenom=$_POST['prenom'];
		$phone=$_POST['phone'];
		$password=$_POST['password'];
		$confpwd=$_POST['conf_password'];
		$type='gerant';

		$secret=rand(0,9999999);
		
		
		$photo=addslashes($_FILES['photo']['name']);
		$size=$_FILES['photo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($photo,'.'),1));
		
		
		$req_select=$bdd->prepare('SELECT * FROM admin WHERE login=:log');
		$req_select->execute(array('log'=>$email));
		$n=$req_select->rowCount();
		
		if($n==0){
			
			if(!empty($photo)){
				if($size>2048000)
				{
					echo '<span style="color: red; font-size: 20px">image trop grand</span>';
				}
				else
				{
					if(in_array($extenson_image, $extension_valides)){
						$dossier= 'images/profile/';
						$upload_image=$_FILES['photo']['tmp_name'];
						move_uploaded_file($upload_image,$dossier.$photo);
							
					}
				}
			}
			else
			{
				$photo='user.png';
			}
			

			if($password==$confpwd){
				if(strlen($password)>=6){
					$crypted_pwd=sha1(sha1($password));
			
					$req_insert=$bdd->prepare('INSERT INTO admin (ID, nom, boutique, telephone, prenom, login, password, email, photo, type, secret, activation) VALUES (:id, :nom, :bout, :tel, :pren, :log, :pass, :mail, :photo, :type, :secret, :act)');
					$req_insert->execute(array(
											'id'=>NULL,
											'nom'=>$nom,
											'bout'=>$boutique,
											'tel'=>$phone,
											'pren'=>$prenom,
											'log'=>$email,
											'pass'=>$crypted_pwd,
											'mail'=>$email,
											'photo'=>$photo,
											'type'=>$type,
											'secret'=>$secret,
											'act'=>1
										));
					
					
					if($req_insert){
						$erreur='
							<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<i class="mdi mdi-check-all"></i>
								<strong>Gerant ajouté avec succés !</strong>
							</div>';
					}
					else{
						$erreur='
							<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<i class="mdi mdi-block-helper"></i>
								<strong>Oups!</strong> Echec de l\'inscription. Veuillez réessayer !!!
							</div>';
					}
				}
				else{
					$erreur='
						<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert"
									aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<i class="mdi mdi-block-helper"></i>
							<strong>Erreur!</strong> Le mot de passe doit contenir au moins 6 caractères !!!
						</div>';
				}
				
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Mots de passe non identiques.
					</div>';
			}
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Cet email est déjà associé à un compte.
				</div>';
		}
	}
	
//-----------Desactiver un gerant

	if(isset($_POST['desactive_geran']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
        $activation1=$_POST['activation'];

        if($activation1==0){
           $activation=1;
        }else{
        	$activation=0;
        }
		$req_supp=$bdd->prepare('UPDATE admin SET activation=:activation WHERE secret=:sec');
		$req_supp->execute(array(
			                      'activation'=>$activation,
			                      'sec'=>$secret
			                       ));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-success alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Statut du gerant modifié avec succés <?=$activation?></strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la modification de la statut. Réessayer!.
					</div>';
			}
	}
	

//----------- ajouter un administrateur

	if(isset($_POST['register_admin'])){
		$erreur='';
		$email=$_POST['email'];
		$nom=$_POST['nom'];
		$prenom=$_POST['prenom'];
		$phone=$_POST['phone'];
		$password=$_POST['password'];
		$confpwd=$_POST['conf_password'];
		$type='admin';
		$boutique=0;

		$secret=rand(0,9999999);
		
		
		$photo=addslashes($_FILES['photo']['name']);
		$size=$_FILES['photo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($photo,'.'),1));
		
		
		$req_select=$bdd->prepare('SELECT * FROM admin WHERE login=:log');
		$req_select->execute(array('log'=>$email));
		$n=$req_select->rowCount();
		
		if($n==0){
			
			if(!empty($photo)){
				if($size>2048000)
				{
					echo '<span style="color: red; font-size: 20px">image trop grand</span>';
				}
				else
				{
					if(in_array($extenson_image, $extension_valides)){
						$dossier= 'images/profile/';
						$upload_image=$_FILES['photo']['tmp_name'];
						move_uploaded_file($upload_image,$dossier.$photo);
							
					}
				}
			}
			else
			{
				$photo='user.png';
			}
			

			if($password==$confpwd){
				if(strlen($password)>=6){
					
					$crypted_pwd=sha1(sha1($password));
			
					$req_insert=$bdd->prepare('INSERT INTO admin (ID, nom, boutique, telephone, prenom, login, password, email, photo, type, secret, activation) VALUES (:id, :nom, :btk, :tel, :pren, :log, :pass, :mail, :photo, :type, :secret, :act)');
					$req_insert->execute(array(
											'id'=>NULL,
											'nom'=>$nom,
											'btk'=>$boutique,
											'tel'=>$phone,
											'pren'=>$prenom,
											'log'=>$email,
											'pass'=>$crypted_pwd,
											'mail'=>$email,
											'photo'=>$photo,
											'type'=>$type,
											'secret'=>$secret,
											'act'=>1
										));
					
					
					if($req_insert){
						$erreur='
							<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<i class="mdi mdi-check-all"></i>
								<strong>Admin ajouté avec succés !</strong>
							</div>';
					}
					else{
						$erreur='
							<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<i class="mdi mdi-block-helper"></i>
								<strong>Oups!</strong> Echec de l\'inscription. Veuillez réessayer !!!
							</div>';
					}
				}
				else{
					$erreur='
						<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert"
									aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<i class="mdi mdi-block-helper"></i>
							<strong>Erreur!</strong> Le mot de passe doit contenir au moins 6 caractères !!!
						</div>';
				}
				
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Mots de passe non identiques.
					</div>';
			}
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Cet email est déjà associé à un compte.
				</div>';
		}
	}



//-----------modification mot de passe--------------------

	if (isset($_POST['edit_password'])){
	
	$secret=$_POST['secret'];
	$password=$_POST['password'];
	$password1=$_POST['pass'];
	$new_password=$_POST['pwd_news'];
	$confirme_password=$_POST['conf_pwd'];
	//$crypt='secret';
				$pass=sha1(sha1($password1));
          //puis on teste si le mot de passe et le confirmation de mot de passe sont identique
			if($pass==$password){
				//Enfin on teste si le mot de passe est supÃ©rieur au 6 caracteres
				if(strlen($password1)>=6){
					if($new_password==$confirme_password){
				$crypt='aicha';
				$new_pass=sha1(sha1($new_password));	
				$req_new=$bdd->prepare('UPDATE admin set password=:password WHERE secret=:secret');
					$req_new->execute(array(
					'secret'=>$secret,
					'password'=>$new_pass
					)); if($req_new){
							 $erreur='<div class="alert alert-icon alert-info alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<i class="mdi mdi-block-helper"></i>
									<strong>Modification de mot de passe réussie</strong><br>Veuillez vous connecter avec vos nouveau identifiants
								</div>';
								unset($_SESSION['admin_connexion']);
							 }
							 else{
							 $erreur='erreur ';
							 }
			
				}
				else{
					$erreur='<div class="alert alert-icon alert-info alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<i class="mdi mdi-block-helper"></i>
									<strong>Votre mot de passe et  la confirmation de passe ne correspond pas </strong> 
								</div>';
					}
				}
				else{
					$erreur='<div class="alert alert-icon alert-info alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<i class="mdi mdi-block-helper"></i>
									<strong>Votre nouveau mot de passe doit dépasser au moins 6 caracters </strong> 
								</div>';
					}
				 }
				else{
					$erreur='<div class="alert alert-icon alert-info alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<i class="mdi mdi-block-helper"></i>
									<strong>l\'ancien mot de passe est incorrect..</strong> 
								</div> ';
					}
				
				}

	
	



	//----------- ajouter un membre de l'équipe

	if(isset($_POST['register_member'])){
		$erreur='';
		$email=$_POST['email'];
		$nom=$_POST['nom'];
		$telephone=$_POST['telephone'];
		$prenom=$_POST['prenom'];
		$fonction=$_POST['fonction'];
		$pres=$_POST['presentation'];
		$secret=rand(0,9999999);
		
		
		$photo=addslashes($_FILES['photo']['name']);
		$size=$_FILES['photo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($photo,'.'),1));
		
		
		$req_select=$bdd->prepare('SELECT * FROM personnel WHERE email=:email');
		$req_select->execute(array('email'=>$email));
		$n=$req_select->rowCount();
		
		if($n==0){
			
			if(!empty($photo)){
				if($size>2048000)
				{
					echo '<span style="color: red; font-size: 20px">image trop grand</span>';
				}
				else
				{
					if(in_array($extenson_image, $extension_valides)){
						$dossier= 'images/';
						$upload_image=$_FILES['photo']['tmp_name'];
						move_uploaded_file($upload_image,$dossier.$photo);
							
					}
				}
			}
			else
			{
				$photo='user.png';
			}
			
			$req_insert=$bdd->prepare('INSERT INTO personnel (ID, photo, nom, prenom, telephone, fonction, email, description, secret, date) VALUES (:id, :photo, :nom, :pren, :tel, :fonction, :mail, :descrip, :secret, :dat)');
			$req_insert->execute(array(
									'id'=>NULL,
									'photo'=>$photo,
									'nom'=>$nom,
									'pren'=>$prenom,
									'tel'=>$telephone,
									'fonction'=>$fonction,
									'mail'=>$email,
									'descrip'=>$pres,
									'secret'=>$secret,
									'dat'=>$date
								));
			
			
			if($req_insert){
				$erreur='
					<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-check-all"></i>
						<strong>Membre ajouté avec succés !</strong>
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Oups!</strong> Echec de l\'inscription. Veuillez réessayer !!!
					</div>';
			}
				
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Erreur:</strong> Cet email est déjà associé à un compte.
				</div>';
		}
	}
	
	//Delete product

	if(isset($_POST['delete_membre']))
	{
			
		$erreur='';
		$secret=$_POST['secret'];
													
		$req_supp=$bdd->prepare('DELETE FROM membre WHERE secret=:sec');
		$req_supp->execute(array('sec'=>$secret));
							
			if($req_supp){
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong> suppréssion réussie</strong>.
					</div>';
			}
			else{
				$erreur='
					<div class="alert alert-icon alert-danger alert-dismissible fade in text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<i class="mdi mdi-block-helper"></i>
						<strong>Erreur:</strong> Echec de la suppression. Réessayer!.
					</div>';
			}
	}
	
	
	

	



	//----------------envoi un mail -------------------------

	if (isset($_POST['envoyer_mail'])){
	
		$objet= $_POST['objet'];
		$email_src= $_POST['email_src'];
		$email_dest= $_POST['email_dest'];
		$message_mail= $_POST['message'];
		$secret=rand(0,66666);
		$destinataire=$email_dest;
	    $type='envoi';
		$lecture=0;
		// Pour les champs $expediteur / $copie / $destinataire, sÃ©parer par une virgule s'il y a plusieurs adresses
		$message=$message_mail;
		
		$headers = 'From: '.$email_src.'' . "\r\n" .
			 'Reply-To: '.$email_src.'' . "\r\n" .
			 'X-Mailer: PHP/' . phpversion();
			 
		if (mail($destinataire, $objet, $message_mail, $headers)) // Envoi du message
		{
			$erreur_mail='Votre message a été bien envoyé';
			
			$req_insert=$bdd->prepare('INSERT INTO messagerie (	ID,email,name,objet,message,secret,type,lecture,date) VALUES (:ID,:email,:name,:objet,:message,:secret,:type,:lecture,:date)');
		    $req_insert->execute(array(
								'ID'=>NULL,
								'email'=>$email_dest,
								'name'=>'',
								'objet'=>$objet,
								'message'=>$message_mail,
								'secret'=>$secret,
								'type'=>$type,
								'lecture'=>$lecture,
								'date'=>$date
							));
		}
		else // Non envoyÃ©
		{
			$erreur_mail= 'Votre message n\'a pas pu etre envoyé';
		}
										
	}
	
	

	//----------------Ajouter un Design----------------------------------------

	if(isset($_POST['register_design'])){
		$erreur='';
	
		$secret=rand(0,985549);
		
		$photo=addslashes($_FILES['photo']['name']);
		$size=$_FILES['photo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($photo,'.'),1));
		
		if(!empty($photo)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image, $extension_valides)){
					$dossier= '../img/image_icon/';
					$upload_image=$_FILES['photo']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$photo);
						
				}
			}
		}
		else
		{
			$photo='';
		}
		
		$req_insert=$bdd->prepare('INSERT INTO design (ID, photo,secret) VALUES (:ID, :photo,:secret)');
		$req_insert->execute(array(
								'ID'=>NULL,
								'photo'=>$photo,
								'secret'=>$secret,
								
							));
		if($req_insert){
			$erreur='
				<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-check-all"></i>
					<strong>Design ajoutée avec succés!</strong>
				</div>';
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Oups!</strong> Echec de l\'ajout. Veuillez réessayer !!!
				</div>';
		}
	}
	
	//----------------Modifier un Design----------------------------------------

	if(isset($_POST['edit_design'])){
		$erreur='';
	
		$secret=$_POST['secret'];
		$photo1=$_POST['photo'];
		$photo=addslashes($_FILES['photo']['name']);
		$size=$_FILES['photo']['size'];
		$extension_valides= array('png', 'jpg', 'jpeg','jpe');
		$extenson_image= strtolower(substr(strrchr($photo,'.'),1));
		
		if(!empty($photo)){
			if($size>2048000)
			{
				echo '<span style="color: red; font-size: 20px">image trop grand</span>';
			}
			else
			{
				if(in_array($extenson_image, $extension_valides)){
					$dossier= '../img/image_icon/';
					$upload_image=$_FILES['photo']['tmp_name'];
					move_uploaded_file($upload_image,$dossier.$photo);
						
				}
			}
		}
		else
		{
			$photo=$photo1;
		}
		
		
		$req_insert=$bdd->prepare('UPDATE design SET photo=:photo WHERE secret=:secret');
		$req_insert->execute(array(
								'photo'=>$photo,
								'secret'=>$secret
							));
		if($req_insert){
			$erreur='
				<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-check-all"></i>
					<strong>Modification réussie!</strong>
				</div>';
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Oups!</strong> Echec de la mise à jour. Veuillez réessayer !!!
				</div>';
		}
	}
	
	//----------------Supprimer un Design----------------------------------------

	if(isset($_POST['supp_design'])){
		$erreur='';
	
		$secret=$_POST['secret'];
		
		$req_insert=$bdd->prepare('DELETE FROM design  WHERE secret=:secret');
		$req_insert->execute(array(
								'secret'=>$secret
							));
		if($req_insert){
			$erreur='
				<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-check-all"></i>
					<strong>Suppréssion réussie!</strong>
				</div>';
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Oups!</strong> Echec de la suppréssion. Veuillez réessayer !!!
				</div>';
		}
	}
	//----------------Supprimer une commande----------------------------------------

	if(isset($_POST['supp_commande'])){
		$erreur='';
	
		$secret=$_POST['secret'];
		
		$req_insert=$bdd->prepare('DELETE FROM expedition  WHERE secret=:secret');
		$req_insert->execute(array(
								'secret'=>$secret
							));
							
		$req_add=$bdd->prepare('DELETE FROM add_tshirt  WHERE secret_expedition=:secret_expedition');
		$req_add->execute(array(
								'secret_expedition'=>$secret
							));
		if($req_insert){
			$erreur='
				<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-check-all"></i>
					<strong>Suppréssion réussie!</strong>
				</div>';
		}
		else{
			$erreur='
				<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<i class="mdi mdi-block-helper"></i>
					<strong>Oups!</strong> Echec de la suppréssion. Veuillez réessayer !!!
				</div>';
		}
	}
	
	

?>