<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Modification d'une modèle!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Modèle</a></li>
                </ol>
            </div>
        </div>
        <?php
            if (isset($erreur)) {
                echo $erreur;
            }
            if(isset($_GET['id_'])){
                $reference=$_GET['id_'];
        ?>
        <!-- row -->
        <div class="row">
            <div class="col-xl-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Formulaire pour modifier une modele</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <?php
                            $req_select=$bdd->prepare('SELECT * FROM modeles WHERE reference=:reference');
                            $req_select->execute(array('reference'=>$reference));
                            
                            $donnees=$req_select->fetch(PDO::FETCH_ASSOC);
                            $titre=$donnees['titre'];
                            $prix=$donnees['prix'];
                            $photo=$donnees['photo'];
                            $description=$donnees['description'];
                            ?>
                            <form method="POST" action="" enctype="multipart/form-data">

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Titre</label>
                                        <input type="text" name="titre" style="color:green" value="<?= $titre ?>" class="form-control" placeholder="Titre">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Prix</label>
                                        <input type="number" name="prix" style="color:green" value="<?= $prix ?>" class="form-control" placeholder="prix">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea style="color:green"  class="form-control" name="description"><?= $description ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Photo de la modèle</label>
                                    <input type="file" name="photo" class="form-control">
                                    <input type="hidden"  name="photo" value="<?php echo $photo ?>">
                                    <label style="color:green"><?php echo $photo ?><label>
                                </div>
                                <input type="hidden" name="reference" value="<?=$reference?>">
                                <button type="submit" name="edit_modele" class="btn btn-primary">Valider</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>