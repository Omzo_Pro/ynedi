<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Ajout d'une tenue!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Conceptstore</a></li>
                </ol>
            </div>
        </div>
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }
        if(isset($_GET['id_'])){
            $matricule=$_GET['id_'];
        ?>
        <!-- row -->
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Formulaire pour ajouter une tenue</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <?php
                            $req_tenue=$bdd->prepare('SELECT * FROM tenues WHERE matricule=:mat');
                            $req_tenue->execute(array('mat'=>$matricule));
                            $donnees_tenue=$req_tenue->fetch(PDO::FETCH_ASSOC);
                            $modele=$donnees_tenue['modele'];
                            $qte=$donnees_tenue['quantite'];
                            $taille=$donnees_tenue['taille'];
                            $prix=$donnees_tenue['prix'];
                            $description=$donnees_tenue['description'];
                            $date=$donnees_tenue['dateRegister'];

                                $req_cli=$bdd->prepare('SELECT * FROM modeles where reference=:mat');
                                $req_cli->execute(array('mat'=>$modele));
                                $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
                                $photo=$donnees_cli['photo'];
                                $titre=$donnees_cli['titre'];
                                $prixy=$donnees_cli['prix'];
                            ?>
                            <form method="POST" action="" enctype="multipart/form-data">

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Modele</label>
                                        <img src="images/modeles/<?=$photo?>" width="100px" alt="<?=$titre?>">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Quantité</label>
                                        <input type="number" name="qte" value="<?=$qte?>" class="form-control" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Taille</label>
                                        <input type="text" name="taille" class="form-control" value="<?=$taille?>" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Prix Concepstore</label>
                                        <input type="number" name="prix" class="form-control" value="<?=$prix?>">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Date</label>
                                        <input type="date" name="dateR" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description"></textarea>
                                </div>
                                <button type="submit" name="add_tenue" class="btn btn-primary">Valider</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>