<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Ajout d'un Tailleur!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Tailleurs</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <?php
            if (isset($erreur)) {
                echo $erreur;
            }
            if(isset($_GET['id_'])){
                $matricule=$_GET['id_'];
        ?>
        <div class="row">
			<div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Formulaire pour modifier un tailleur</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <?php
                            $req_select=$bdd->prepare('SELECT * FROM tailleur WHERE matricule=:mat');
                            $req_select->execute(array('mat'=>$matricule));
                            
                            $donnees=$req_select->fetch(PDO::FETCH_ASSOC);
                            $id=$donnees['idTailleur'];
                            $prenom=$donnees['prenom'];
                            $nom=$donnees['nom'];;
                            $email=$donnees['email'];
                            $adresse=$donnees['adresse'];
                            $recto=$donnees['idrecto'];
                            $verso=$donnees['idverso'];
                            $telephone=$donnees['telephone'];
                            $matTailleur=$donnees['matricule'];
                            ?>
                            <form method="POST" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Prenom</label>
                                        <input type="text" name="prenom" style="color:green" class="form-control" value="<?=$prenom?>" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Nom</label>
                                        <input type="text" name="nom" style="color:green" class="form-control" value="<?=$nom?>" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Adresse</label>
                                        <input type="text" name="adresse" value="<?=$adresse?>" style="color:green" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Telephone</label>
                                        <input type="text" name="telephone" value="<?=$telephone?>" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Email</label>
                                        <input type="email" name="email" style="color:green" class="form-control" value="<?=$email?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Photo recto carte d'idenité</label>
                                        <input type="file" name="recto" value="<?=$recto?>" class="form-control">
                                        <label style="color:green"><?=$recto ?><label>
                                    </div>
                                    <div class="form-group">
                                        <label>Photo verso carte d'idenité</label>
                                        <input type="file" name="verso" value="<?=$verso?>" class="form-control">
                                        <label style="color:green"><?=$verso ?><label>
                                    </div>
                                </div>
                                <input type="hidden" name="matricule" value="<?=$matTailleur?>">
                                <button type="submit" name="edit_tailleur" class="btn btn-primary">Valider</button>
                            </form>
                        </div>
                    </div>
                </div>
			</div>
        </div>
        <?php } ?>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>