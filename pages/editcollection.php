<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Ajout d'une collection!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Collection</a></li>
                </ol>
            </div>
        </div>
        <?php
            if (isset($erreur)) {
                echo $erreur;
            }
            if(isset($_GET['id_'])){
                $secret=$_GET['id_'];
        ?>
        <!-- row -->
        <div class="row">
            
            <?php include('includes/carousel.php'); ?>
            <div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Formulaire pour modifier une collection</h4>
                    </div>
                    <?php
                        $req_collection=$bdd->prepare('SELECT * FROM collection WHERE secret=:sec');
                        $req_collection->execute(array('sec'=>$secret));
                        $donnees=$req_collection->fetch(PDO::FETCH_ASSOC);
                        $collection=$donnees['collection'];
                        $description=$donnees['description'];;
                        $secret=$donnees['secret'];
                    ?>
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="POST" action="" enctype="multipart/form-data">

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label>Titre</label>
                                        <input type="text" name="nom" style="color:green" value="<?=$collection?>" class="form-control" placeholder="Titre">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" style="color:green" name="description"><?=$description?></textarea>
                                </div>
                                <input type="hidden" name="secret" value="<?=$secret?>">
                                <button type="submit" name="edit_collection" class="form-control btn btn-primary">Valider</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>