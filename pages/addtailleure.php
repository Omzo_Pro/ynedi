<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Ajout d'un Tailleur!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Tailleurs</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <?php
            if (isset($erreur)) {
                echo $erreur;
            }
        ?>
        <div class="row">
			<div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Formulaire pour ajouter un tailleur</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="POST" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Prenom</label>
                                        <input type="text" name="prenom" class="form-control" placeholder="Prénom" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Nom</label>
                                        <input type="text" name="nom" class="form-control" placeholder="Nom" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Adresse</label>
                                        <input type="text" name="adresse" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Telephone</label>
                                        <input type="text" name="telephone" placeholder="+221 77 777 77 77" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label>Photo recto carte d'idenité</label>
                                        <input type="file" name="recto"  class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Photo verso carte d'idenité</label>
                                        <input type="file" name="verso"  class="form-control">
                                    </div>
                                </div>
                                <button type="submit" name="add_tailleur" class="btn btn-primary">Valider</button>
                            </form>
                        </div>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>