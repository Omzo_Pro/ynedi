<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>liste des modèles!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Modèles</a></li>
                </ol>
            </div>
        </div>
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }

        ?>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
                        <thead>
                            <tr>
                                <th>Modèles</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $req_modele=$bdd->prepare('SELECT * FROM modeles');
                                $req_modele->execute(array());
                                while($donnees=$req_modele->fetch(PDO::FETCH_ASSOC))
                                {
                                $id=$donnees['idModele'];
                                $reference=$donnees['reference'];
                                $titre=$donnees['titre'];
                                $prix=$donnees['prix'];
                                $photo=$donnees['photo'];
                                $description=$donnees['description'];
                            ?>
                            <tr>
                                <td>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row m-b-30">
                                                <div class="col-md-6 col-xxl-6">
                                                    <div class="new-arrival-product mb-4 mb-xxl-4 mb-md-0">
                                                        <div class="new-arrivals-img-contnent">
                                                            <img class="img-fluid" src="images/modeles/<?=$photo?>" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xxl-6">
                                                    <div class="new-arrival-content position-relative">
                                                        <a href="?p=modele&id_=<?=$reference?>"><h4><?=$titre?></h4></a>
                                                        <p class="price"><?=$prix?> Fcfa</p>
                                                        <p>Reference: <span class="item"><?=$reference?></span> </p>
                                                        <p class="text-content"><?=$description?></p>
                                                        <span class="review-text"></span><a class="product-review" href="?p=edit_modele&id_=<?=$reference ?>"><i class="fa fa-pencil scale5 text-warning mr-2"></i>Modifier</a> 
                                                        <a class="product-review" href="#" data-toggle="modal" data-target="#basicModal<?=$reference?>"> <i class="las la-times-circle scale5 text-danger mr-2"></i>Supprimer</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade" id="basicModal<?=$reference?>">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Suppression</h5>
                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">Voulez-vous vraiment supprimer &hellip; ?</div>
                                        <form method="POST" action="">
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger light" data-dismiss="modal">Annuler</button>
                                                <input type="hidden" name="reference" value="<?php echo $reference ?>"/>
                                                <button name="sup_modele" class="btn btn-danger">Confirmer suppression</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>