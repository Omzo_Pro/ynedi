<?php
    if ($connected==1) {
?>
<!--**********************************
    Sidebar start
***********************************-->
<div class="deznav">
    <div class="deznav-scroll">
		<ul class="metismenu" id="menu">
            <li><a href="?p=accueil" class="ai-icon" aria-expanded="false">
                    <i class="flaticon-381-networking"></i>
                    <span class="nav-text">Accueil</span>
                </a>
            </li>
            
            <?php
                
            if (($role=='admin') OR ($role=='super_admin')){
            ?>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="flaticon-381-layer-1"></i>
                    <span class="nav-text">Commandes</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="?p=add_commande&t=c">Ajouter une commande</a></li>
                    <li><a href="?p=list_commande&t=c">Gerer les commandes</a></li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="flaticon-381-layer"></i>
                    <span class="nav-text">Retouche</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="?p=add_retouche&t=r">Ajouter une retouche</a></li>
                    <li><a href="?p=list_commande&t=r">Gerer les retouches</a></li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="fa fa-shield"></i>
                    <span class="nav-text">Collections</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="?p=add_collection">Ajouter une collection</a></li>
                    <li><a href="?p=list_collection">Gerer les collections</a></li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="flaticon-381-diamond"></i>
                    <span class="nav-text">Modeles</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="?p=add_modele">Ajouter une modele</a></li>
                    <li><a href="?p=list_modele">Gerer les modeles</a></li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="fa fa-users"></i>
                    <span class="nav-text">Tailleur</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="?p=add_tailleure">Ajouter un(e) tailleur</a></li>
                    <li><a href="?p=list_tailleure">Gerer les tailleurs</a></li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="fa fa-user"></i>
                    <span class="nav-text">Clients</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="?p=add_client">Ajouter une cliente</a></li>
                    <li><a href="?p=list_client">Gerer les clients</a></li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="fa fa-shield"></i>
                    <span class="nav-text">ConceptStore</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="?p=add_conceptstore">Ajouter ConceptStore</a></li>
                    <li><a href="?p=list_conceptstore">Gerer les ConceptStore</a></li>
                    <li><a href="?p=add_tenue">Ajouter Tenue</a></li>
                </ul>
            </li>
            <?php
                }
            ?>
        </ul>
		<div class="copyright">
			<p><a href="https://carrapidecommunication.com/" target="_blank"><strong>CRC - YNEDI</strong></a> © 2022 All Rights Reserved</p>
		</div>
	</div>
</div>
<!--**********************************
    Sidebar end
***********************************-->
<?php
}
?>