<?php
    if ($connected==1) {

    if(isset($_GET['id_'])){
        $concept=$_GET['id_'];

    $req_concept=$bdd->prepare('SELECT * FROM conceptstores WHERE matricule=:mat');
    $req_concept->execute(array('mat'=>$concept));
    $donnees_concept=$req_concept->fetch(PDO::FETCH_ASSOC);
    $id=$donnees_concept['id'];
    $nom=$donnees_concept['nom'];
    $adresse=$donnees_concept['adresse'];
    $contact=$donnees_concept['contact'];
    $logo=$donnees_concept['logo'];

    

                                
?>
 <!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4></h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Client</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="row">
        	<div class="col-xl-12 col-lg-12 col-sm-12">
				<div class="card overflow-hidden">
					<div class="text-center p-3" style="background-image: url(images/profile/cover.jpg);">
						<div class="profile-photo">
							<img src="images/conceptstore/<?=$logo?>" width="100" class="img-fluid rounded-circle" alt="">
						</div>
						<h3 class="mt-3 mb-1 text-white"><?=$nom?></h3>
						<p class="text-white mb-0"><?=$adresse?></p>
						<p class="text-white mb-0"><?=$contact?></p>
					</div>
                </div>
			</div>
		</div>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
                        <thead>
                            <tr>
                                <th>Tenue</th>
                                <th>Quantité</th>
                                <th>Taille</th>
                                <th>Prix Ynedi</th>
                                <th>Prix Conceptstore</th>
                                <th>date</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $req_tenue=$bdd->prepare('SELECT * FROM tenues WHERE conceptstore=:mat');
                            $req_tenue->execute(array('mat'=>$concept));
                            while($donnees_tenue=$req_tenue->fetch(PDO::FETCH_ASSOC)){
                            $modele=$donnees_tenue['modele'];
                            $qte=$donnees_tenue['quantite'];
                            $taille=$donnees_tenue['taille'];
                            $prix=$donnees_tenue['prix'];
                            $matricule=$donnees_tenue['matricule'];
                            $description=$donnees_tenue['description'];
                            $date=$donnees_tenue['dateRegister'];

                                $req_cli=$bdd->prepare('SELECT * FROM modeles where reference=:mat');
                                $req_cli->execute(array('mat'=>$modele));
                                $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
                                $photo=$donnees_cli['photo'];
                                $titre=$donnees_cli['titre'];
                                $prixy=$donnees_cli['prix'];
                            ?>
                            <tr>
                                <td>
                                    <center><?=$titre?></center>
                                    <a href="?p=modele&id_=<?=$modele?>"><img src="images/modeles/<?=$photo?>" width="100px" alt="<?=$titre?>"></a><hr>
                                    
                                </td>
                                <td><?=$qte?></td>
                                <td><?=$taille?></td>
                                <td><?=$prixy?> Fcfa</td>
                                <td><?=$prix?> Fcfa</td>
                                <td><?=$date?></td>
                                <td><?=$description?></td>
                                <td>
                                    <div class="dropdown ml-auto text-right">
                                        <div class="btn-link" data-toggle="dropdown">
                                            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                                        </div>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="product-review" href="?p=edit_tenue&id_=<?=$matricule ?>"><i class="fa fa-pencil scale5 text-warning mr-2"></i>Modifier</a>
                                            <a class="dropdown-item" href="#"><i class="las la-times-circle scale5 text-danger mr-2"></i> Supprimer</a>
                                            
                                        </div>
                                    </div>
                                </td>                                               
                            </tr>
                            <?php
                                }
                            ?>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</div>
</div>
<?php 
    }
}
else{
    include 'pages/login.php';
}
?>