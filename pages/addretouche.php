<?php
    if ($connected==1) {
?>
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Ajout d'une Retouche!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Retouche</a></li>
                </ol>
            </div>
        </div>
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }

        ?>
        <!-- row -->
        <div class="row">
            
            <!-- Column starts -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h4 class="card-title">Enregistrement des retouchess</h4>
                    </div>
                    <div class="card-body">
                        <div id="accordion-nine" class="accordion accordion-active-header">
                            <div class="accordion__item">
                                <div class="accordion__header" data-toggle="collapse" data-target="#active-header_collapseOne">
                                    <span class="accordion__header--icon"></span>
                                    <span class="accordion__header--text">Cliente existente</span>
                                    <span class="accordion__header--indicator"></span>
                                </div>
                                <div id="active-header_collapseOne" class="collapse accordion__body show" data-parent="#accordion-nine">
                                    <form method="POST" action="">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Formulaire pour une cliente existente</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="modele" class="text-label">Cliente</label>
                                                        <select id="single-select" name="client" class="form-control form-control-lg" required>
                                                            <?php
                                                                $req_tailleur=$bdd->prepare('SELECT * FROM clients');
                                                                $req_tailleur->execute(array());
                                                                while($donnees=$req_tailleur->fetch(PDO::FETCH_ASSOC))
                                                                {
                                                                $prenom=$donnees['prenom'];
                                                                $nom=$donnees['nom'];
                                                                $matricule=$donnees['matricule'];
                                                            ?>
                                                            <option value="<?=$matricule?>"><?=$prenom?> <?=$nom?></option>
                                                            <?php
                                                             }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php
                                                    $req_modele=$bdd->prepare('SELECT * FROM modeles');
                                                    $req_modele->execute(array());

                                                ?>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="modele" class="text-label">Modele</label>
                                                        <select class="multi-select form-control form-control-lg" name="modele[]" multiple="multiple" required>
                                                            <?php
                                                                while($donnees=$req_modele->fetch(PDO::FETCH_ASSOC))
                                                                {
                                                                $reference=$donnees['reference'];
                                                                $titre=$donnees['titre'];
                                                                $prix=$donnees['prix'];
                                                            ?>
                                                            <option value="<?=$reference?>"><?=$titre?> (<?=$prix?> Fcfa)</option>
                                                            <?php
                                                             }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="text-label">Note</label>
                                                        <textarea class="form-control" name="note"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="text-label">Delais de livraison</label>
                                                        <input type="number" name="delai" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="text-label">Avance</label>
                                                        <input type="number" name="avance" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="text-label">Remise</label>
                                                        <input type="number" name="remise" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <input type="hidden" name="type" value="retouche">
                                                        <input type="submit" class="btn btn-primary form-control" value="valider" name="add_commande1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="accordion__item">
                                <div class="accordion__header collapsed" data-toggle="collapse" data-target="#active-header_collapseTwo">
                                    <span class="accordion__header--icon"></span>
                                    <span class="accordion__header--text">Nouvelle cliente</span>
                                    <span class="accordion__header--indicator"></span>
                                </div>
                                <div id="active-header_collapseTwo" class="collapse accordion__body" data-parent="#accordion-nine">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title">Formulaire pour une nouvelle cliente</h4>
                                            </div>
                                            <div class="card-body">
                                            <form method="POST" action="" id="step-form-horizontal" class="step-form-horizontal">
                                                <div style="heigh:100px" >
                                                    <h4>Infos de la cliente</h4>
                                                    <section>
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <label for="modele" class="text-label">Infos de la cliente</label>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-lg-12 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Prénom*</label>
                                                                            <input type="text" name="prenom" class="form-control" placeholder="Prenom" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Nom*</label>
                                                                            <input type="text" name="nom" class="form-control" placeholder="Nom" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Jour et moi de naissance</label>
                                                                            <input type="date" name="naissance" class="form-control" placeholder="21/01" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 mb-3">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Commentaire</label>
                                                                            <select name="commentaire" class="form-control">
                                                                                <option value="Internet">Internet</option>
                                                                                <option value="Instagram">Instagram</option>
                                                                                <option value="Boutique">Boutique</option>
                                                                                <option value="Reference">Reference</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <h4>Contact de la cliente</h4>
                                                    <section>
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <label for="modele" class="text-label">Contact de la cliente</label>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="col-lg-12 mb-2">
                                                                    <div class="form-group">
                                                                        <label class="text-label">Adresse</label>
                                                                        <input type="text" name="adresse" class="form-control" placeholder="Adresse" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 mb-2">
                                                                    <div class="form-group">
                                                                        <label class="text-label">Adresse Email</label>
                                                                        <input type="email" class="form-control" id="inputGroupPrepend2" name="email" aria-describedby="inputGroupPrepend2" placeholder="example@example.com" required>
                                                                    </div>
                                                                </div>
                                                                 <div class="col-lg-12 mb-2">
                                                                    <div class="form-group">
                                                                        <label class="text-label">Numéro de Téléphone*</label>
                                                                        <input type="text" name="telephone" class="form-control" placeholder="+221772222222" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="text-label">Whatsapp</label>
                                                                        <input type="number" name="whatsapp" class="form-control" placeholder="221772222222">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <h4>Mesures</h4>
                                                    <section class="h-auto">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <label for="modele" class="text-label">Mesures de la cliente</label>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <label class="text-label">Epaule</label>
                                                                        <input class="form-control"  type="number" name="epaule" value="0" placeholder="epaule" >
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Poitrine</label>
                                                                            <input type="number" name="poitrine" class="form-control" value="0" placeholder="poitrine" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Taille</label>
                                                                            <input type="number" name="taille" class="form-control" value="0" placeholder="taille" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Ceinture</label>
                                                                            <input type="number" name="ceinture" class="form-control" value="0" placeholder="ceinture" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Hanche</label>
                                                                            <input type="number" name="hanche" class="form-control" value="0" placeholder="hanche" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Blouse</label>
                                                                            <input type="number" name="blouse" class="form-control" value="0" placeholder="blouse" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Cou</label>
                                                                            <input type="number" name="cou" class="form-control" value="0" placeholder="cou" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Manche courte</label>
                                                                            <input type="number" name="mancheCourte" class="form-control" value="0" placeholder="Manche courte" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Manche 3/4</label>
                                                                            <input type="number" name="manche34" class="form-control" value="0" placeholder="Manche 3/4" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Manche longue</label>
                                                                            <input type="number" name="mancheLongue" class="form-control" value="0" placeholder="Manche longue" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">T.manche</label>
                                                                            <input type="number" name="tmanche" class="form-control" value="0" placeholder="tmanche" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Poignet</label>
                                                                            <input type="number" name="poignet" class="form-control" value="0" placeholder="poignet" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Cuisse</label>
                                                                            <input type="number" name="cuisses" class="form-control" value="0" placeholder="cuisse" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Long jupe</label>
                                                                            <input type="number" name="ljupe" class="form-control" value="0" placeholder="long jupe" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Jupe 3/4</label>
                                                                            <input type="number" name="jupe34" class="form-control" value="0" placeholder="Jupe 3/4" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Long pantalon</label>
                                                                            <input type="number" name="lpantalon" class="form-control" value="0" placeholder="Long pantalon" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Long robe</label>
                                                                            <input type="number" name="lrobe" class="form-control" value="0" placeholder="Long robe" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Long Robe 3/4</label>
                                                                            <input type="number" name="longRobe34" class="form-control" value="0" placeholder="Long Robe 3/4" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 col-sm-3 mb-2">
                                                                        <div class="form-group">
                                                                            <label class="text-label">Long Boubou</label>
                                                                            <input type="number" name="lboubou" class="form-control" value="0" placeholder="Long Boubou" >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <h4>Modele</h4>
                                                    <section>
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <label for="modele" class="text-label">Modele</label>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label for="modele" class="text-label">Modele</label>
                                                                        <select class="multi-select form-control form-control-lg" name="modele[]" multiple="multiple">
                                                                            <?php
                                                                                $req_modele=$bdd->prepare('SELECT * FROM modeles');
                                                                                $req_modele->execute(array());
                                                                                while($donnees=$req_modele->fetch(PDO::FETCH_ASSOC))
                                                                                {
                                                                                $reference=$donnees['reference'];
                                                                                $titre=$donnees['titre'];
                                                                            ?>
                                                                            <option value="<?=$reference?>"><?=$titre?></option>
                                                                            <?php
                                                                             }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label class="text-label">Note</label>
                                                                        <textarea class="form-control" name="note"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label class="text-label">Delais de livraison</label>
                                                                        <input type="number" name="delai" class="form-control" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label class="text-label">Avance</label>
                                                                        <input type="number" name="avance" class="form-control" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label class="text-label">Remise</label>
                                                                        <input type="number" name="remise" class="form-control" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <input type="hidden" name="type" value="retouche">
                                                                        <input type="submit" class="btn btn-primary form-control" value="valider" name="add_commande">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column ends -->
        </div>
    </div>
</div>
<?php
}
else{
    include 'pages/login.php';
}
?>