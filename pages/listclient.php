<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Liste des Clients!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Clients</a></li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Email</th>
                                <th>Téléphone</th>
                                <th>Whatsapp</th>
                                <th class=" pl-5" style="min-width: 200px;">Addresse
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="customers">
                            <?php
                                $req_modele=$bdd->prepare('SELECT * FROM clients');
                                $req_modele->execute(array());
                                while($donnees=$req_modele->fetch(PDO::FETCH_ASSOC))
                                {
                                $prenom=$donnees['prenom'];
                                $nom=$donnees['nom'];;
                                $email=$donnees['email'];
                                $adresse=$donnees['adresse'];
                                $telephone=$donnees['telephone'];
                                $whatsapp=$donnees['whatsapp'];
                                $matClient=$donnees['matricule'];
                                $verification=$donnees['verification'];
                            ?>
                            <tr class="btn-reveal-trigger">
                                <td class="py-3">
                                    <a href="#">
                                        <div class="media d-flex align-items-center">
                                            
                                            <div class="media-body">
                                                <h5 class="mb-0 fs--1"><a href="?p=client&id_=<?=$matClient?>"><?=$prenom?> <?=$nom?></a></h5>
                                            </div>
                                        </div>
                                    </a>
                                </td>
                                <td class="py-2"><a
                                        href="mailto:<?=$email?>"><?=$email?></a></td>
                                <td class="py-2"> <a href="tel:<?=$telephone?>"><?=$telephone?></a>
                                <td><a class="btn btn-success" target="_blank" href="https://api.whatsapp.com/send?phone=<?=$whatsapp?>&amp;text=Bonjour, votre commande est disponible" class="button button-mini button-circle button-green"><i class="fa fa-whatsapp"></i> </a></td>
                                <td class="py-2 pl-5"><?=$adresse?></td>
                                <td class="py-2 text-right">
                                    <div class="dropdown"><button class="btn btn-primary tp-btn-light sharp" type="button" data-toggle="dropdown"><span class="fs--1"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg></span></button>
                                        <div class="dropdown-menu dropdown-menu-right border py-0">
                                            <div class="py-2">
                                                <a class="dropdown-item"  href="?p=edit_client&id_=<?=$matClient?>">Modifier</a>
                                                <a class="dropdown-item text-danger" href="#!" data-toggle="modal" data-target="#basicModal<?=$matClient?>">Supprimer</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade" id="basicModal<?=$matClient?>">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Suppression</h5>
                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">Voulez-vous vraiment supprimer &hellip; ?</div>
                                        <form method="POST" action="">
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger light" data-dismiss="modal">Annuler</button>
                                                <input type="hidden" name="matricule" value="<?=$matClient ?>"/>
                                                <button name="sup_cli" class="btn btn-danger">Confirmer suppression</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>