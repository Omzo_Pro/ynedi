<?php
  if ($connected==1) {
?>
<!--**********************************
    Footer start
***********************************-->
<div class="footer">
    <div class="copyright">
        <p>Copyright © Designed &amp; Developed by <a href="https://carrapidecommunication.com/" target="_blank">CRC</a> 2022</p>
    </div>
</div>
<!--**********************************
    Footer end
***********************************-->
<?php
}
?>
<!--**********************************
   Support ticket button start
***********************************-->

<!--**********************************
   Support ticket button end
***********************************-->


</div>
<!--**********************************
Main wrapper end
***********************************-->

<!--**********************************
Scripts
***********************************-->
<!-- Required vendors -->
<script src="./vendor/global/global.min.js"></script>
<script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="./vendor/chart.js/Chart.bundle.min.js"></script>
<script src="./js/custom.min.js"></script>
<script src="./js/deznav-init.js"></script>

<!-- Counter Up -->
<script src="./vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="./vendor/jquery.counterup/jquery.counterup.min.js"></script>	

<!-- Apex Chart -->
<script src="./vendor/apexchart/apexchart.js"></script>	

<!-- Chart piety plugin files -->
<script src="./vendor/peity/jquery.peity.min.js"></script>

<!-- Dashboard 1 -->
<script src="./js/dashboard/dashboard-1.js"></script>

<script src="./vendor/jquery-steps/build/jquery.steps.min.js"></script>
<script src="./vendor/jquery-validation/jquery.validate.min.js"></script>
<!-- Form validate init -->
<script src="./js/plugins-init/jquery.validate-init.js"></script>



<!-- Form step init -->
<script src="./js/plugins-init/jquery-steps-init.js"></script>


<!-- Datatable -->
<script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

<script src="./js/plugins-init/datatables.init.js"></script>

<script src="./vendor/select2/js/select2.full.min.js"></script>
<script src="./js/plugins-init/select2-init.js"></script>
<script src="./vendor/dropzone/dist/dropzone.js"></script>
</body>
</html>