<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Ajout d'une Cliente</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Clientes</a></li>
                </ol>
            </div>
        </div>
        <?php 
            if (isset($erreur)) {
                echo $erreur;
            }
        ?>
        <!-- row -->
        <div class="row">
			<div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Formulaire ajout cliente</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="POST" action="">
                                <h4>Infos de la cliente</h4>
                                <section>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-12 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Prénom*</label>
                                                        <input type="text" name="prenom" class="form-control" placeholder="Prenom" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Nom*</label>
                                                        <input type="text" name="nom" class="form-control" placeholder="Nom" required>
                                                    </div>
                                                </div>
                                                 <div class="col-lg-12 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Jour et moi de naissance</label>
                                                        <input type="date" name="naissance" class="form-control" placeholder="21/01" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 mb-3">
                                                    <div class="form-group">
                                                        <label class="text-label">Commentaire</label>
                                                        <select name="commentaire" class="form-control">
                                                            <option value="Internet">Internet</option>
                                                            <option value="Instagram">Instagram</option>
                                                            <option value="Boutique">Boutique</option>
                                                            <option value="reference">Reference</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <h4>Contact de la cliente</h4>
                                <section>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="col-lg-12 mb-2">
                                                <div class="form-group">
                                                    <label class="text-label">Adresse</label>
                                                    <input type="text" name="adresse" class="form-control" placeholder="Adresse" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 mb-2">
                                                <div class="form-group">
                                                    <label class="text-label">Adresse Email</label>
                                                    <input type="email" class="form-control" id="inputGroupPrepend2" name="email" aria-describedby="inputGroupPrepend2" placeholder="example@example.com" required>
                                                </div>
                                            </div>
                                             <div class="col-lg-12 mb-2">
                                                <div class="form-group">
                                                    <label class="text-label">Numéro de Téléphone*</label>
                                                    <input type="text" name="telephone" class="form-control" placeholder="+221772222222" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="text-label">Whatsapp</label>
                                                    <input type="number" name="whatsapp" class="form-control" placeholder="221772222222">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <h4>Mesures</h4>
                                <section class="h-auto">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <label class="text-label">Epaule</label>
                                                    <input class="form-control"  type="number" name="epaule" value="0" placeholder="epaule" >
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Poitrine</label>
                                                        <input type="number" name="poitrine" class="form-control" value="0" placeholder="poitrine" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Taille</label>
                                                        <input type="number" name="taille" class="form-control" value="0" placeholder="taille" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Ceinture</label>
                                                        <input type="number" name="ceinture" class="form-control" value="0" placeholder="ceinture" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Hanche</label>
                                                        <input type="number" name="hanche" class="form-control" value="0" placeholder="hanche" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Blouse</label>
                                                        <input type="number" name="blouse" class="form-control" value="0" placeholder="blouse" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Cou</label>
                                                        <input type="number" name="cou" class="form-control" value="0" placeholder="cou" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Manche courte</label>
                                                        <input type="number" name="mancheCourte" class="form-control" value="0" placeholder="Manche courte" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Manche 3/4</label>
                                                        <input type="number" name="manche34" class="form-control" value="0" placeholder="Manche 3/4" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Manche longue</label>
                                                        <input type="number" name="mancheLongue" class="form-control" value="0" placeholder="Manche longue" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">T.manche</label>
                                                        <input type="number" name="tmanche" class="form-control" value="0" placeholder="tmanche" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Poignet</label>
                                                        <input type="number" name="poignet" class="form-control" value="0" placeholder="poignet" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Cuisse</label>
                                                        <input type="number" name="cuisses" class="form-control" value="0" placeholder="cuisse" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Long jupe</label>
                                                        <input type="number" name="ljupe" class="form-control" value="0" placeholder="long jupe" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Jupe 3/4</label>
                                                        <input type="number" name="jupe34" class="form-control" value="0" placeholder="Jupe 3/4" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Long pantalon</label>
                                                        <input type="number" name="lpantalon" class="form-control" value="0" placeholder="Long pantalon" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Long robe</label>
                                                        <input type="number" name="lrobe" class="form-control" value="0" placeholder="Long robe" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Long Robe 3/4</label>
                                                        <input type="number" name="longRobe34" class="form-control" value="0" placeholder="Long Robe 3/4" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Long Boubou</label>
                                                        <input type="number" name="lboubou" class="form-control" value="0" placeholder="Long Boubou" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <button type="submit" name="addclient" class="btn btn-primary form-control">Enregistrer</button>
                            </form>
                        </div>
                    </div>
                </div>
			</div>
        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>