<?php
    if ($connected==1) {
        if(isset($_GET['ref'])){
                $referenceCom=$_GET['ref'];

        $req_cmd=$bdd->prepare('SELECT * FROM commandes WHERE reference=:ref');
        $req_cmd->execute(array('ref'=>$referenceCom));
        $donnees_cmd=$req_cmd->fetch(PDO::FETCH_ASSOC);
        $modele=$donnees_cmd['modele'];
        $modele2 = json_decode($modele);
        $client=$donnees_cmd['client'];
        $tailleur=$donnees_cmd['tailleur'];
        $type=$donnees_cmd['type'];
        $note=$donnees_cmd['note'];
        $avance=$donnees_cmd['avance'];
        $remise=$donnees_cmd['remise'];
        $dateCommande=$donnees_cmd['dateCommande'];
        $dateLivraison=$donnees_cmd['dateLivraison'];
        $delaiLivraison=$donnees_cmd['delaiLivraison'];

        $req_cli_act=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
        $req_cli_act->execute(array('mat'=>$client));
        $donnees_cli_act=$req_cli_act->fetch(PDO::FETCH_ASSOC);
        $prenomCli=$donnees_cli_act['prenom'];
        $nomCli=$donnees_cli_act['nom'];
        $matClient=$donnees_cli_act['matricule'];

        $req_tail_act=$bdd->prepare('SELECT * FROM tailleur where matricule=:mat');
        $req_tail_act->execute(array('mat'=>$tailleur));
        $donnees_tail_act=$req_tail_act->fetch(PDO::FETCH_ASSOC);
        $prenomTail=$donnees_tail_act['prenom'];
        $nomTail=$donnees_tail_act['nom'];
        $matTailleur=$donnees_tail_act['matricule'];
?>
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Modification de <?=$type?>!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href=""><?=$type?></a></li>
                </ol>
            </div>
        </div>
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }      
        ?>
        <!-- row -->
        <div class="row">
            
            <!-- Column starts -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h4 class="card-title">Modification de <?=$type?></h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="">
                            <div class="card">
                                <div class="card-body">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="modele" class="text-label">Modeles</label>
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Titre</th>
                                                            <th class="right">Description</th>
                                                            <th class="right">Prix</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php
                                                        $i=0; $total=0;
                                                        if (is_array($modele2) || is_object($modele2)){
                                                            foreach ($modele2 as $mod) {
                                                            $req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
                                                            $req_modele->execute(array('ref'=>$mod));
                                                            $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
                                                            $titre=$donnees_mod['titre'];
                                                            $referenceModele=$donnees_mod['reference'];
                                                            $prix=$donnees_mod['prix'];
                                                            $prixx=(number_format($prix,0,".","."));
                                                            $description=$donnees_mod['description'];
                                                            $photo=$donnees_mod['photo'];
                                                            $total+=$prix;
                                                            $i++;

                                                        ?>
                                                        <tr>
                                                            <td><a href="?p=modele&id_=<?=$mod?>"><img src="images/modeles/<?=$photo?>" width="100px" alt="<?=$titre?>"></a></td>
                                                            <td class="left strong"><strong><?=$titre?></strong></td>
                                                            <td><?=$description?></td>
                                                            <td><?=$prixx?> Fcfa</td>
                                                        </tr>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        $req_modele=$bdd->prepare('SELECT * FROM modeles');
                                        $req_modele->execute(array());

                                    ?>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="modele" class="text-label">Modele</label>
                                            <select class="multi-select form-control form-control-lg" name="modele[]" multiple="multiple" required>
                                                <?php
                                                    while($donnees=$req_modele->fetch(PDO::FETCH_ASSOC))
                                                    {
                                                    $reference=$donnees['reference'];
                                                    $titre=$donnees['titre'];
                                                    $prix=$donnees['prix'];
                                                ?>
                                                <option value="<?=$reference?>"><?=$titre?> (<?=$prix?> Fcfa)</option>
                                                <?php
                                                 }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="modele" class="text-label">Tailleur</label>
                                            <select id="single-select" name="tailleur" class="form-control form-control-lg">
                                                <option value="<?=$matTailleur?>"><?=$prenomTail?> <?=$nomTail?></option>
                                                <?php
                                                    $req_tailleur=$bdd->prepare('SELECT * FROM tailleur');
                                                    $req_tailleur->execute(array());
                                                    while($donnees=$req_tailleur->fetch(PDO::FETCH_ASSOC))
                                                    {
                                                    $prenom=$donnees['prenom'];
                                                    $nom=$donnees['nom'];
                                                    $matricule=$donnees['matricule'];
                                                ?>
                                                <option value="<?=$matricule?>"><?=$prenom?> <?=$nom?></option>
                                                <?php
                                                 }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="text-label">Note</label>
                                            <textarea class="form-control" name="note" style="color:green"><?=$note?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="text-label">Delais de livraison</label>
                                            <input type="number" name="delai" value="<?=$delaiLivraison?>" class="form-control" style="color:green">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="text-label">Avance</label>
                                            <input type="number" name="avance" style="color:green" value="<?=$avance?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="text-label">Remise</label>
                                            <input type="number" style="color:green" name="remise" value="<?=$remise?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="hidden" name="reference" value="<?=$referenceCom?>">
                                            <input type="submit" class="btn btn-primary form-control" value="valider" name="edit_commande">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Column ends -->
        </div>
    </div>
</div>
<?php
    }
}
else{
    include 'pages/login.php';
}
?>