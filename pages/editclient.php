<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Modification d'une Cliente</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Cliente</a></li>
                </ol>
            </div>
        </div>
        <?php
            if (isset($erreur)) {
                echo $erreur;
            }
            if(isset($_GET['id_'])){
                $secret=$_GET['id_'];
        ?>
        <!-- row -->
        <div class="row">
			<div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Formulaire Modification de cliente</h4>
                    </div>
                    <?php
                        $req_client=$bdd->prepare('SELECT * FROM clients WHERE matricule=:mat');
                        $req_client->execute(array('mat'=>$secret));
                        $donnees_cli=$req_client->fetch(PDO::FETCH_ASSOC);
                        $prenom=$donnees_cli['prenom'];
                        $nom=$donnees_cli['nom'];;
                        $email=$donnees_cli['email'];
                        $adresse=$donnees_cli['adresse'];
                        $telephone=$donnees_cli['telephone'];
                        $matClient=$donnees_cli['matricule'];
                        $id=$donnees_cli['idClient'];

                        $commentaire=$donnees_cli['commentaire'];
                        $whatsapp=$donnees_cli['whatsapp'];
                        $jourNaiss=$donnees_cli['jourNaiss'];
                        $moisNaiss=$donnees_cli['moisNaiss'];

                        $req_mesure=$bdd->prepare('SELECT * FROM mesures WHERE client=:mat');
                        $req_mesure->execute(array('mat'=>$matClient));
                        $donnees_mes=$req_mesure->fetch(PDO::FETCH_ASSOC);
                        $epaule=$donnees_mes['epaule'];
                        $poitrine=$donnees_mes['poitrine'];
                        $taille=$donnees_mes['taille'];
                        $ceinture=$donnees_mes['ceinture'];
                        $hanche=$donnees_mes['hanche'];
                        $blouse=$donnees_mes['blouse'];
                        $manche34=$donnees_mes['manche34'];
                        $mancheLongue=$donnees_mes['mancheLongue'];
                        $mancheCourte=$donnees_mes['mancheCourte'];
                        $cou=$donnees_mes['cou'];
                        $tManches=$donnees_mes['tManches'];
                        $poignet=$donnees_mes['poignet'];
                        $cuisses=$donnees_mes['cuisses'];
                        $longJupe=$donnees_mes['longJupe'];
                        $jupe34=$donnees_mes['jupe34'];
                        $longPantalon=$donnees_mes['longPantalon'];
                        $longRobe=$donnees_mes['longRobe'];
                        $longRobe34=$donnees_mes['longRobe34'];
                        $longBoubou=$donnees_mes['longBoubou'];

                    ?>
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="POST" action="">
                                <h4>Infos de la cliente</h4>
                                <section>
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="modele" class="text-label">Infos de la cliente</label>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-12 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Prénom*</label>
                                                        <input type="text" style="color:green" name="prenom" value="<?=$prenom?>" class="form-control" placeholder="Prenom">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Nom</label>
                                                        <input type="text" style="color:green" name="nom" value="<?=$nom?>" class="form-control" placeholder="Nom">
                                                    </div>
                                                </div>
                                                 <div class="col-lg-12 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Jour et moi de naissance</label>
                                                        <input type="number" style="color:green" name="jourNaiss" class="form-control" value="<?=$jourNaiss?>" >
                                                        <input type="number" style="color:green" name="moisNaiss" class="form-control" value="<?=$moisNaiss?>" >
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 mb-3">
                                                    <div class="form-group">
                                                        <label class="text-label">Commentaire</label>
                                                        <select name="commentaire" class="form-control">
                                                            <option style="color:green" value="<?=$commentaire?>"><?=$commentaire?></option>
                                                            <option value="Internet">Internet</option>
                                                            <option value="Instagram">Instagram</option>
                                                            <option value="Boutique">Boutique</option>
                                                            <option value="Reference">Reference</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <h4>Contact de la cliente</h4>
                                <section>
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="modele" class="text-label">Contact de la cliente</label>
                                        </div>
                                        <div class="card-body">
                                            <div class="col-lg-12 mb-2">
                                                <div class="form-group">
                                                    <label class="text-label">Adresse</label>
                                                    <input type="text" style="color:green" name="adresse" value="<?=$adresse?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 mb-2">
                                                <div class="form-group">
                                                    <label class="text-label">Adresse Email</label>
                                                    <input type="email" value="<?=$email?>" class="form-control" style="color:green" name="email">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 mb-2">
                                                <div class="form-group">
                                                    <label class="text-label">Numéro de Téléphone*</label>
                                                    <input type="text" style="color:green" name="telephone" class="form-control" value="<?=$telephone?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 mb-2">
                                                <div class="form-group">
                                                    <label class="text-label">Numéro Whatsapp*</label>
                                                    <input type="text" style="color:green" name="whatsapp" class="form-control" value="<?=$whatsapp?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <h4>Mesures</h4>
                                <section class="h-auto">
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="modele" class="text-label">Mesures de la cliente</label>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <label class="text-label">Epaule</label>
                                                    <input class="form-control"  type="number" style="color:green" name="epaule" value="<?=$epaule?>" placeholder="epaule" >
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Poitrine</label>
                                                        <input type="number" style="color:green" name="poitrine" class="form-control" value="<?=$poitrine?>" placeholder="poitrine" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Taille</label>
                                                        <input type="number" style="color:green" name="taille" class="form-control" value="<?=$taille?>" placeholder="taille" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Ceinture</label>
                                                        <input type="number" style="color:green" name="ceinture" class="form-control" value="<?=$ceinture?>" placeholder="ceinture" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Hanche</label>
                                                        <input type="number" style="color:green" name="hanche" class="form-control" value="<?=$hanche?>" placeholder="hanche" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Blouse</label>
                                                        <input type="number" style="color:green" name="blouse" class="form-control" value="<?=$blouse?>" placeholder="blouse" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Cou</label>
                                                        <input type="number" style="color:green" name="cou" class="form-control" value="<?=$cou?>" placeholder="cou" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Manche 3/4</label>
                                                        <input type="number" style="color:green" name="manche34" class="form-control" value="<?=$manche34?>" placeholder="Manche 3/4" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Manche longue</label>
                                                        <input type="number" style="color:green" name="mancheLongue" class="form-control" value="<?=$mancheLongue?>" placeholder="Manche longue" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Manche courte</label>
                                                        <input type="number" style="color:green" name="mancheCourte" class="form-control" value="<?=$mancheCourte?>" placeholder="Manche courte" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">T.manche</label>
                                                        <input type="number" style="color:green" name="tmanche" class="form-control" value="<?=$tManches?>" placeholder="tmanche" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Poignet</label>
                                                        <input type="number" style="color:green" name="poignet" class="form-control" value="<?=$poignet?>" placeholder="poignet" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Cuisse</label>
                                                        <input type="number" style="color:green" name="cuisses" class="form-control" value="<?=$cuisses?>" placeholder="cuisse" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Long jupe</label>
                                                        <input type="number" style="color:green" name="ljupe" class="form-control" value="<?=$longJupe?>" placeholder="long jupe" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Jupe 3/4</label>
                                                        <input type="number" style="color:green" name="jupe34" class="form-control" value="<?=$jupe34?>" placeholder="Jupe 3/4" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Long pantalon</label>
                                                        <input type="number" style="color:green" name="lpantalon" class="form-control" value="<?=$longPantalon?>" placeholder="Long pantalon" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Long robe</label>
                                                        <input type="number" style="color:green" name="lrobe" class="form-control" value="<?=$longRobe?>" placeholder="Long robe" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Long Robe 3/4</label>
                                                        <input type="number" style="color:green" name="longRobe34" class="form-control" value="<?=$longRobe34?>" placeholder="Long Robe 3/4" >
                                                    </div>
                                                </div>
                                                <div class="col-4 col-sm-3 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Long Boubou</label>
                                                        <input type="number" style="color:green" name="lboubou" class="form-control" value="<?=$longBoubou?>" placeholder="Long Boubou" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <input type="hidden" name="matricule" value="<?=$matClient?>">
                                <button type="submit" name="updateclient" class="btn btn-primary form-control">Enregistrer</button>
                            </form>
                        </div>
                    </div>
                </div>
			</div>
        </div>
        <?php } ?>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>