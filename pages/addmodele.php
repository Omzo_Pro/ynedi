<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Ajout d'une modèle!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Modèle</a></li>
                </ol>
            </div>
        </div>
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }

        ?>
        <!-- row -->
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Formulaire pour ajouter une modele</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="POST" action="" enctype="multipart/form-data">

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Collection</label>
                                        <select name="collection" class="form-control">
                                            <?php
                                                $req_collection=$bdd->prepare('SELECT * FROM collection');
                                                $req_collection->execute(array());
                                                while($donnees=$req_collection->fetch(PDO::FETCH_ASSOC))
                                                {
                                                $secret=$donnees['secret'];
                                                $collection=$donnees['collection'];
                                            ?>
                                            <option value="<?=$secret?>"><?=$collection?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Titre</label>
                                        <input type="text" name="titre" class="form-control" placeholder="Titre">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Prix</label>
                                        <input type="number" name="prix" class="form-control" placeholder="prix">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Photo de la modèle</label>
                                    <input type="file" name="photo"  class="form-control">
                                </div>
                                <div class="dropzone">
                                    <label>Photo de la modèle</label>
                                    <div class="fallback">
                                        <input name="file" type="file" multiple />
                                    </div>
                                </div>
                                <button type="submit" name="add_modele" class="btn btn-primary">Valider</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>