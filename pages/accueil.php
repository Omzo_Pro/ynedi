<?php
	if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <!-- row -->
	<div class="container-fluid">
		<div class="form-head d-flex mb-3 align-items-start">
			<div class="mr-auto d-none d-lg-block">
				<h2 class="text-black font-w600 mb-0">Accueil</h2>
				<p class="mb-0">Bienvenue sur Ynedi Admin !</p>
			</div>
		</div>
		<?php
        if (($role=='admin') OR ($role=='super_admin')){
        	include('includes/admin_accueil.php');
			}
			else{
				include('includes/tailor_accueil.php');
			}
		?>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
	include 'pages/login.php';
}
?>