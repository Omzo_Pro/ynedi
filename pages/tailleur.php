<?php
    if ($connected==1) {

    if(isset($_GET['id_'])){
        $tailleur=$_GET['id_'];

    $req_tailleur=$bdd->prepare('SELECT * FROM tailleur WHERE matricule=:mat');
    $req_tailleur->execute(array('mat'=>$tailleur));
    $donnees=$req_tailleur->fetch(PDO::FETCH_ASSOC);
    $prenom=$donnees['prenom'];
    $nom=$donnees['nom'];
    $email=$donnees['email'];
    $adresse=$donnees['adresse'];
    $photo=$donnees['profile'];
    $recto=$donnees['idrecto'];
    $verso=$donnees['idverso'];
    $telephone=$donnees['telephone'];
    $matricule=$donnees['matricule'];
                                    
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4></h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Tailleur</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="profile card card-body px-3 pt-3 pb-0">
                    <div class="profile-head">
                        <div class="photo-content">
                            <div class="cover-photo"></div>
                        </div>
                        <div class="profile-info">
							<div class="profile-photo">
								<img src="images/profile/<?=$photo?>" class="img-fluid rounded-circle" alt="">
							</div>
							<div class="profile-details">
								<div class="profile-name px-3 pt-2">
									<h4 class="text-primary mb-0"><?=$prenom?> <?=$nom?></h4>
									<p>Tailleur</p>
								</div>
								<div class="profile-email px-2 pt-2">
                                    <p>Email</p>
									<h4 class="text-muted mb-0"><?=$email?></h4>
								</div>
                                <div class="profile-phone px-2 pt-2">
                                    <p>Téléphone</p>
                                    <h4 class="text-muted mb-0"><?=$telephone?></h4>
                                </div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-body">
                        <div class="profile-tab">
                            <div class="custom-tab-1">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item"><a href="#my-posts" data-toggle="tab" class="nav-link active show">Posts</a>
                                    </li>
                                    <li class="nav-item"><a href="#about-me" data-toggle="tab" class="nav-link">Tenue cousues</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="my-posts" class="tab-pane fade active show">
                                        <div class="my-post-content pt-3">
                                            <div class="post-input">
                                                <img src="images/tailleur/<?=$recto?>" alt="" style="width:300px;">
                                                <img src="images/tailleur/<?=$verso?>" alt="" style="width:300px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="about-me" class="tab-pane fade">
                                        <div class="table-responsive">
                                            <table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
                                                <thead>
                                                    <tr>
                                                        <th>reference</th>
                                                        <th>Modele</th>
                                                        <th>Client</th>
                                                        <th>Type</th>
                                                        <th>date enregistré</th>
                                                        <th>date de livraison</th>
                                                        <th>etat</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $req_cmd=$bdd->prepare('SELECT * FROM commandes WHERE tailleur=:tail');
                                                        $req_cmd->execute(array('tail'=>$tailleur));
                                                        while($donnees_cmd=$req_cmd->fetch(PDO::FETCH_ASSOC))
                                                        {
                                                        $id=$donnees_cmd['idCommande'];
                                                        $reference=$donnees_cmd['reference'];
                                                        $modele=$donnees_cmd['modele'];
                                                        $modele2 = json_decode($modele);
                                                        $client=$donnees_cmd['client'];
                                                        $type=$donnees_cmd['type'];
                                                        $tailleur=$donnees_cmd['tailleur'];
                                                        $dateCommande=$donnees_cmd['dateCommande'];
                                                        $dateConception=$donnees_cmd['dateConception'];
                                                        $dateEnd=$donnees_cmd['dateEnd'];
                                                        $dateLivraison=$donnees_cmd['dateLivraison'];
                                                        $etat=$donnees_cmd['etat']; 
                                                        $btn="";
                                                        $link="";
                                                        $newetat="";
                                                        if ($etat=="enregistré") {
                                                            $link="Conception";
                                                            $btn="warning";
                                                            $newetat="en Conception";
                                                        }elseif ($etat=="en Conception") {
                                                            $link="Terminé";
                                                            $btn="primary";
                                                            $newetat="prêt à être livrée";
                                                        }elseif ($etat=="prêt à être livrée") {
                                                            $btn="success";
                                                            $newetat="prêt à être livrée";
                                                        }

                                                        $req_cli=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
                                                        $req_cli->execute(array('mat'=>$client));
                                                        $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
                                                        $prenomCli=$donnees_cli['prenom'];
                                                        $nomCli=$donnees_cli['nom'];
                                                        $matClient=$donnees_cli['matricule'];

                                                    ?>
                                                    <tr>
                                                        <td><a href="?p=commande&id_=<?=$reference?>">#<?=$reference?></a></td>
                                                        <td>
                                                            <?php
                                                            if (is_array($modele2) || is_object($modele2)){
                                                                foreach ($modele2 as $mod) {
                                                                $req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
                                                                $req_modele->execute(array('ref'=>$mod));
                                                                $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
                                                                $titre=$donnees_mod['titre'];
                                                                $referenceModele=$donnees_mod['reference'];
                                                                $photo=$donnees_mod['photo'];
                                                                ?>
                                                            <a href="?p=modele&id_=<?=$mod?>"><img src="images/modeles/<?=$photo?>" width="100px" alt="<?=$referenceModele?>"></a>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <td><a href="?p=client&id_=<?=$matClient?>"><?=$prenomCli?> <?=$nomCli?></a></td>
                                                        <td><?=$type?></td>
                                                        <td><?=$dateCommande?></td>
                                                        <td><?=$dateLivraison?></td>
                                                        <td><span class="btn btn-sm light btn-<?=$btn?> fs-16"><?=$etat?></span></td>
                                                        <td>
                                                            <div class="dropdown ml-auto text-right">
                                                                <div class="btn-link" data-toggle="dropdown">
                                                                    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
                                                                </div>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <?php
                                                                        if ($role=='cheftailor'){
                                                                    ?>
                                                                    <form method="POST" action="">
                                                                        <input type="hidden" name="reference" value="<?=$reference?>">
                                                                        <input type="hidden" name="dat1" value="<?=$dateConception?>">
                                                                        <input type="hidden" name="dat2" value="<?=$dateEnd?>">
                                                                        <input type="hidden" name="etat" value="<?=$newetat?>">
                                                                        <button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i> <?=$link?></button>
                                                                    </form>
                                                                    <?php
                                                                        }
                                                                        if (($role=='admin') OR ($role=='super_admin')){
                                                                    ?>
                                                                    <a class="dropdown-item" href="#"><i class="fa fa-pencil scale5 text-warning mr-2"></i> Modifier</a>
                                                                    <a class="dropdown-item" href="#"><i class="las la-times-circle scale5 text-danger mr-2"></i> Supprimer</a>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </td>                                               
                                                    </tr>
                                                    <?php
                                                        }
                                                    ?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <!--**********************************
    Content body end
***********************************-->
<?php 
    }
}
else{
    include 'pages/login.php';
}
?>