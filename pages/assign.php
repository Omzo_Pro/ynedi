<?php
	if ($connected==1) {
?>
<div class="content-body">
    <!-- row -->
	<div class="container-fluid">
		<div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Liste des commandes!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Commande</a></li>
                </ol>
            </div>
        </div>
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }

        ?>
        <!-- row -->
        <div class="row">
			<div class="col-12">
				<div class="table-responsive">
					<table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
						<thead>
							<tr>
								<th>reference</th>
								<th>Modele</th>
								<th>Client</th>
								<th>date enregistré</th>
								<th>date de livraison</th>
								<th>Tailleur</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
		              $req_cmd=$bdd->prepare('SELECT * FROM commandes WHERE tailleur=0');
		              $req_cmd->execute(array());
		              while($donnees_cmd=$req_cmd->fetch(PDO::FETCH_ASSOC))
		              {
		             	$id=$donnees_cmd['idCommande'];
		              $reference=$donnees_cmd['reference'];
		              $modele=$donnees_cmd['modele'];
		              $modele2=json_decode($donnees_cmd['modele']);
		              $client=$donnees_cmd['client'];
		              $dateCommande=$donnees_cmd['dateCommande'];
		              $dateConception=$donnees_cmd['dateConception'];
		              $dateEnd=$donnees_cmd['dateEnd'];
		              $dateLivraison=$donnees_cmd['dateLivraison'];

		              $req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
		              $req_modele->execute(array('ref'=>$modele));
		              $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
		              $titre=$donnees_mod['titre'];
		              $referenceModele=$donnees_mod['reference'];
		              $photo=$donnees_mod['photo'];

		              $req_cli=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
		              $req_cli->execute(array('mat'=>$client));
		              $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
		              $prenomCli=$donnees_cli['prenom'];
		              $nomCli=$donnees_cli['nom'];
		              $matClient=$donnees_cli['matricule'];

		          ?>
		          
							<tr>
								<td><a href="?p=commande&id_=<?=$reference?>">#<?=$reference?></a></td>
								<td>
									<?php
									if (is_array($modele2) || is_object($modele2)){
										foreach ($modele2 as $mod) {
										$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
						                $req_modele->execute(array('ref'=>$mod));
						                $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
						                $titre=$donnees_mod['titre'];
						                $referenceModele=$donnees_mod['reference'];
						                $photo=$donnees_mod['photo'];
									?>
									<div>
										<a href="?p=modele&id_=<?=$referenceModele?>"><img src="images/modeles/<?=$photo?>" width="100px"></a>
									</div>
									<?php
										}
									}
									?>
								</td>
								<td><a href="?p=client&id_=<?=$matClient?>"><?=$prenomCli?> <?=$nomCli?></a></td>
								<td><?=$dateCommande?></td>
								<td><?=$dateLivraison?></td>
								<td>
								<form method="POST" action="">
										<div class="form-group">
				              <select name="tailor">
				              	<?php
                            $req_modele=$bdd->prepare('SELECT * FROM tailleur WHERE activation=1');
                            $req_modele->execute(array());
                            while($donnees=$req_modele->fetch(PDO::FETCH_ASSOC))
                            {
                            $id=$donnees['idTailleur'];
                            $prenom=$donnees['prenom'];
                            $nom=$donnees['nom'];;
                            $matricule=$donnees['matricule'];
                        ?>
				              	<option value="<?=$matricule?>"><?=$prenom?> <?=$nom?></option>
				              	<?php } ?>
				              </select>
				            </div>
				            <div class="form-group">
											<input type="hidden" name="reference" value="<?=$reference?>">
											<button class="btn btn-primary" name="assign">Assigner</button>
				            </div>
								</form>
				        </td>					
							</tr>
							
							<?php
								}
							?>
							
						</tbody>
					</table>
				</div>
      </div>
		</div>
    </div>
</div>
<?php
}
else{
	include 'pages/login.php';
}
?>