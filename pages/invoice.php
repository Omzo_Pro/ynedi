<?php
  if ($connected==1) {
?>

<?php
if(isset($_GET['ref_'])){
        $reference=$_GET['ref_'];
        $req_cmd=$bdd->prepare('SELECT * FROM commandes WHERE reference=:ref');
    $req_cmd->execute(array('ref'=>$reference));
    $donnees_cmd=$req_cmd->fetch(PDO::FETCH_ASSOC);
    $id=$donnees_cmd['idCommande'];
    $reference=$donnees_cmd['reference'];
    $type=$donnees_cmd['type'];
    $modele=$donnees_cmd['modele'];
    $modele2 = json_decode($modele);
    $client=$donnees_cmd['client'];
    $idCommande=$donnees_cmd['idCommande'];
    $note=$donnees_cmd['note'];
    $paiement=$donnees_cmd['paiement'];
    $dateConception=$donnees_cmd['dateConception'];
    $dateEnd=$donnees_cmd['dateEnd'];
    $dateLivraison=$donnees_cmd['dateLivraison'];
    $remise=$donnees_cmd['remise'];
    
    //$date=$donnees['date'];
    $year=substr($dateLivraison, 0, 4);
    $month=substr($dateLivraison, 5, 2);
    $day=substr($dateLivraison, 8, 2);


    $req_cli=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
    $req_cli->execute(array('mat'=>$client));
    $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
    $prenomCli=$donnees_cli['prenom'];
    $nomCli=$donnees_cli['nom'];
    $adresseCli=$donnees_cli['adresse'];
    $telephoneCli=$donnees_cli['telephone'];
    $emailCli=$donnees_cli['email'];
    $matClient=$donnees_cli['matricule'];


    }
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
         <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Facturation</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Clientes</a></li>
                </ol>
            </div>
        </div>
        <div class="row" id="printableArea">
            <div class="col-lg-12">
                <div class="card mt-3">
                    <div class="card-header"> Facture #<?=$idCommande?> <strong><?=$day?>/<?=$month?>/<?=$year?></strong> <span class="float-right">
                            <strong>Statut:</strong> <?=$paiement?></span> </div>
                    <div class="card-body">
                        <div class="row mb-5">
                            <div class="mt-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                
                                <div><img src="images/ynedi.png" style="width: 100px;"></div>
                                <div>Sicap liberté 1</div>
                                <div>Dakar 11000</div>
                                <div>Email: ynedidkr@gmail.com</div>
                                <div>Tél: +221 77 106 35 36</div>
                            </div>
                            <div class="mt-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <h6>Cliente:</h6>
                                <div> <strong><?=$prenomCli?> <?=$nomCli?></strong> </div>
                                <div><?=$adresseCli?></div>
                                <div><?=$emailCli?></div>
                                <div><?=$telephoneCli?></div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Designation</th>
                                        <th>Qté</th>
                                        <th class="right">Prix Unitaire</th>
                                        <th class="right">Montant</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $i=0; $total=0; $totalremise=0; $totalTTC=0;
                                    if (is_array($modele2) || is_object($modele2)){
                                        foreach ($modele2 as $mod) {
                                        $req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
                                        $req_modele->execute(array('ref'=>$mod));
                                        $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
                                        $titre=$donnees_mod['titre'];
                                        $referenceModele=$donnees_mod['reference'];
                                        $prix=$donnees_mod['prix'];
                                        $prixx=(number_format($prix,0,".","."));
                                        $description=$donnees_mod['description'];
                                        $photo=$donnees_mod['photo'];
                                        $total+=$prix;
                                        $i++;

                                    ?>
                                    <tr>
                                        <td class="left strong"><strong><?=$titre?></strong><br><?=$description?></td>
                                        <td class="left">1</td>
                                        <td class="right"><?=$prixx?> Fcfa</td>
                                        <td class="right"><?=$prixx?> Fcfa</td>
                                    </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-sm-5"> </div>
                            <div class="col-lg-4 col-sm-5 ml-auto">
                                <table class="table table-clear">
                                    <?php 
                                    $totall=(number_format($total,0,".","."));
                                    $totalremise=($total*$remise)/100;
                                    $totalTTC=$total-$totalremise;

                                    $totalremisee=(number_format($totalremise,0,".","."));
                                    $totalTTCc=(number_format($totalTTC,0,".","."));
                                    ?>
                                    <tbody>
                                        <tr>
                                            <td class="left"><strong>Total HT</strong></td>
                                            <td class="right"><?=$totall?> Fcfa</td>
                                        </tr>
                                        <tr>
                                            <td class="left"><strong>Remise (<?=$remise?>%)</strong></td>
                                            <td class="right"><?=$totalremisee?> Fcfa</td>
                                        </tr>
                                        <tr>
                                            <td class="left"><strong>Total TTC</strong></td>
                                            <td class="right"><strong><?=$totalTTCc?> Fcfa</strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-print">
            <div class="col-xs-12">
            <button onclick="printDiv('printableArea')" target="_blank" class="btn btn-default pull-right"><i class="fa fa-print"></i> Imprimer</button>
            </div>
        </div>
    </div>
</div>
<script>
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>