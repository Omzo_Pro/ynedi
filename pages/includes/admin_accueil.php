
<div class="row">
	<?php include('carousel.php'); ?>
	<div class="col-xl-12">
        <div class="card">
            <div class="card-header d-block">
                <h4 class="card-title">Tableau de bord</h4>
            </div>
            <div class="card-body">
                <div id="accordion-seven" class="accordion accordion-header-bg accordion-bordered">
                    <div class="accordion__item">
                        <?php
				            $req_select_dash1=$bdd->prepare('SELECT * FROM commandes WHERE etat="prêt à être livrée"');
				            $req_select_dash1->execute(array());
				            $c=$req_select_dash1->rowCount();
				        ?>
                        <div class="d-flex order-manage p-3 align-items-center mb-4" data-toggle="collapse" data-target="#header-bg_collapseOne">
							<div href="" class="btn fs-22 py-1 btn-success px-4 mr-3"><?=$c?></div>
							<h4 class="mb-0">Commandes prêt à être livrée<i class="fa fa-circle text-success ml-1 fs-15"></i></h4>
                        </div>
                        <div id="header-bg_collapseOne" class="collapse accordion__body show" data-parent="#accordion-seven">
                            <div class="table-responsive">
								<table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
									<thead>
										<tr>
											<th>reference</th>
											<th>Modele</th>
											<th>Client</th>
											<th>Tailleur</th>
											<th>etat</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php
							                while($donnees_cmd=$req_select_dash1->fetch(PDO::FETCH_ASSOC))
							                {
							                $id=$donnees_cmd['idCommande'];
							                $reference=$donnees_cmd['reference'];
							                $modele=$donnees_cmd['modele'];
							                $modele2 = json_decode($modele);
							                $client=$donnees_cmd['client'];
							                $tailleur=$donnees_cmd['tailleur'];

							                $etat=$donnees_cmd['etat'];
							                $btn="";
							                $link="";
							                $newetat="";

							                if ($etat=="enregistrée") {
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Conception</button>';
							                	$btn="warning";
							                	$input="";
							                }elseif ($etat=="en Conception") {
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Terminer</button>';
							                	$btn="primary";
							                	$input="";
							                }if ($etat=="terminée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>valider</button>';
							                	$input="";
							                	$newetat="prête à être livrée";
							                }elseif ($etat=="prête à être livrée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Livrer</button>';
							                	$input="";
							                	$newetat="livrée";
							                }elseif ($etat=="livrée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Cloturer</button>';
							                	$input='
							                	<div class="form-group">
							                		<center><label>Reste:</label></center>
							                		<input type="text" disabled="true" class="form-control" style="color:green;" value="'.$reste.' Fcfa"  />
							                	</div>
							                		';
							                	$newetat="close";
							                }			               

							                

							                $req_cli=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
							                $req_cli->execute(array('mat'=>$client));
							                $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
							                $prenomCli=$donnees_cli['prenom'];
							                $nomCli=$donnees_cli['nom'];
							                $matClient=$donnees_cli['matricule'];

							                $req_tail=$bdd->prepare('SELECT * FROM tailleur where matricule=:mat');
							                $req_tail->execute(array('mat'=>$tailleur));
							                $donnees_tail=$req_tail->fetch(PDO::FETCH_ASSOC);
							                $prenomTail=$donnees_tail['prenom'];
							                $nomTail=$donnees_tail['nom'];
							                $matTailleur=$donnees_tail['matricule'];
							            ?>
										<tr>
											<td><a href="?p=commande&id_=<?=$reference?>">#<?=$reference?></a></td>
											<td>
												<?php
												if (is_array($modele2) || is_object($modele2)){
									 
													foreach ($modele2 as $mod) {
													$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
									                $req_modele->execute(array('ref'=>$mod));
									                $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
									                $titre=$donnees_mod['titre'];
									                $referenceModele=$donnees_mod['reference'];
									                $photo=$donnees_mod['photo'];
													?>
												<a href="?p=modele&id_=<?=$mod?>"><img src="images/modeles/<?=$photo?>" width="100px" alt="<?=$titre?>"></a>
												<?php
													}
												}
												?>
											</td>
											<td><a href="?p=client&id_=<?=$matClient?>"><?=$prenomCli?> <?=$nomCli?></a></td>
											<td><a href="?p=profit&id_=<?=$matTailleur?>"><?=$prenomTail?> <?=$nomTail?></a></td>
											<td><span class="btn btn-sm light btn-<?=$btn?> fs-16"><?=$etat?></span></td>
											<td>
												<div class="dropdown ml-auto text-right">
													<div class="btn-link" data-toggle="dropdown">
														<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
													</div>
													<div class="dropdown-menu dropdown-menu-right">
														<form method="POST" action="">
															<input type="hidden" name="reference" value="<?=$reference?>">
															<input type="hidden" name="dat1" value="<?=$dateConception?>">
															<input type="hidden" name="datLiv" value="<?=$dateLivraison?>">
															<input type="hidden" name="dat2" value="<?=$dateEnd?>">
															<input type="hidden" name="etat" value="<?=$newetat?>">
															<?=$input?>
															<?=$link?>
														</form>
														<a class="dropdown-item" href="?p=edit_commande&ref=<?=$reference ?>"><i class="fa fa-pencil scale5 text-warning mr-2"></i> Modifier</a>
														<a class="dropdown-item" href="#" data-toggle="modal" data-target="#basicModal<?=$reference?>"><i class="las la-times-circle scale5 text-danger mr-2"></i> Supprimer</a>
													</div>
												</div>
											</td>											
										</tr>
										<?php
											}
										?>
										
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                    <div class="accordion__item">
                        <?php
                        $date=date('Y-m-d');
                        $req_select_dash2=$bdd->prepare('SELECT * FROM commandes WHERE (etat="en Conception" OR etat="enregistré") AND DATEDIFF( dateLivraison,:date2 )=1');
				            $req_select_dash2->execute(array('date2'=>$date));
				            $c2=$req_select_dash2->rowCount();
				        ?>
                        <div class="d-flex order-manage p-3 align-items-center mb-4" data-toggle="collapse" data-target="#header-bg_collapseTwo">
							<div class="btn fs-22 py-1 btn-danger px-4 mr-3"><?=$c2?></div>
							<h4 class="mb-0">Commandes à livrées dans 1 jours<i class="fa fa-circle text-danger ml-1 fs-15"></i></h4>
                        </div>
                        <div id="header-bg_collapseTwo" class="collapse accordion__body" data-parent="#accordion-seven">
                            <div class="table-responsive">
								<table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
									<thead>
										<tr>
											<th>reference</th>
											<th>Modele</th>
											<th>Client</th>
											<th>Tailleur</th>
											<th>etat</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php
							                while($donnees_cmd=$req_select_dash2->fetch(PDO::FETCH_ASSOC))
							                {
							                $id=$donnees_cmd['idCommande'];
							                $reference=$donnees_cmd['reference'];
							                $modele=$donnees_cmd['modele'];
							                $modele2 = json_decode($modele);
							                $client=$donnees_cmd['client'];
							                $tailleur=$donnees_cmd['tailleur'];
							                $dateCommande=$donnees_cmd['dateCommande'];
							                $dateLivraison=$donnees_cmd['dateLivraison'];

											//$date_debut = "2012-06-01 02:12:51";
											//$date_fin =  "2017-05-12 11:10:00";
											$date=date('Y-m-d H:i:s');
											$date1 = new DateTime($date);
											$date2 = $date1->diff(new DateTime($dateLivraison));
											$date3=$date2->days;
											echo "Il y a ".$date3." jours entre ".$date." et ".$dateLivraison;

											$etat=$donnees_cmd['etat'];
							                $btn="";
							                $link="";
							                $newetat="";

							                if ($etat=="enregistrée") {
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Conception</button>';
							                	$btn="warning";
							                	$input="";
							                }elseif ($etat=="en Conception") {
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Terminer</button>';
							                	$btn="primary";
							                	$input="";
							                }if ($etat=="terminée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>valider</button>';
							                	$input="";
							                	$newetat="prête à être livrée";
							                }elseif ($etat=="prête à être livrée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Livrer</button>';
							                	$input="";
							                	$newetat="livrée";
							                }elseif ($etat=="livrée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Cloturer</button>';
							                	$input='
							                	<div class="form-group">
							                		<center><label>Reste:</label></center>
							                		<input type="text" disabled="true" class="form-control" style="color:green;" value="'.$reste.' Fcfa"  />
							                	</div>
							                		';
							                	$newetat="close";
							                }			   

							                $req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
							                $req_modele->execute(array('ref'=>$modele));
							                $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
							                $titre=$donnees_mod['titre'];
							                $referenceModele=$donnees_mod['reference'];
							                $photo=$donnees_mod['photo'];

							                $req_cli=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
							                $req_cli->execute(array('mat'=>$client));
							                $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
							                $prenomCli=$donnees_cli['prenom'];
							                $nomCli=$donnees_cli['nom'];
							                $matClient=$donnees_cli['matricule'];

							                $req_tail=$bdd->prepare('SELECT * FROM admin where matricule=:mat');
							                $req_tail->execute(array('mat'=>$tailleur));
							                $donnees_tail=$req_tail->fetch(PDO::FETCH_ASSOC);
							                $prenomTail=$donnees_tail['prenom'];
							                $nomTail=$donnees_tail['nom'];
							                $matTailleur=$donnees_tail['matricule'];
							            ?>
										<tr>
											<td><a href="?p=commande&id_=<?=$reference?>">#<?=$reference?></a></td>
											<td>
												<?php
												if (is_array($modele2) || is_object($modele2)){
									 
													foreach ($modele2 as $mod) {
													$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
									                $req_modele->execute(array('ref'=>$mod));
									                $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
									                $titre=$donnees_mod['titre'];
									                $referenceModele=$donnees_mod['reference'];
									                $photo=$donnees_mod['photo'];
													?>
												<a href="?p=modele&id_=<?=$mod?>"><img src="images/modeles/<?=$photo?>" width="100px" alt="<?=$titre?>"></a>
												<?php
													}
												}
												?>
											</td>
											<td><a href="?p=client&id_=<?=$matClient?>"><?=$prenomCli?> <?=$nomCli?></a></td>
											<td><a href="?p=profit&id_=<?=$matTailleur?>"><?=$prenomTail?> <?=$nomTail?></a></td>	
											<td><span class="btn btn-sm light btn-<?=$btn?> fs-16"><?=$etat?></span></td>
											<td>
												<div class="dropdown ml-auto text-right">
													<div class="btn-link" data-toggle="dropdown">
														<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
													</div>
													<div class="dropdown-menu dropdown-menu-right">
														<form method="POST" action="">
															<input type="hidden" name="reference" value="<?=$reference?>">
															<input type="hidden" name="dat1" value="<?=$dateConception?>">
															<input type="hidden" name="datLiv" value="<?=$dateLivraison?>">
															<input type="hidden" name="dat2" value="<?=$dateEnd?>">
															<input type="hidden" name="etat" value="<?=$newetat?>">
															<?=$input?>
															<?=$link?>
														</form>
														<a class="dropdown-item" href="?p=edit_commande&ref=<?=$reference ?>"><i class="fa fa-pencil scale5 text-warning mr-2"></i> Modifier</a>
														<a class="dropdown-item" href="#" data-toggle="modal" data-target="#basicModal<?=$reference?>"><i class="las la-times-circle scale5 text-danger mr-2"></i> Supprimer</a>
													</div>
												</div>
											</td>												
										</tr>
										<?php
											}
										?>
										
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                    <div class="accordion__item">
                        <?php
				            $date=date('Y-m-d');
                        	$req_select_dash3=$bdd->prepare('SELECT * FROM commandes WHERE (etat="en Conception" OR etat="enregistrée") AND (DATEDIFF( dateLivraison,:date2 ) BETWEEN 0 AND 7)');
				            $req_select_dash3->execute(array('date2'=>$date));
				            $c3=$req_select_dash3->rowCount();
				        ?>
                        <div class="d-flex order-manage p-3 align-items-center mb-4" data-toggle="collapse" data-target="#header-bg_collapseThree">
							<div class="btn fs-22 py-1 btn-warning px-4 mr-3"><?=$c3?></div>
							<h4 class="mb-0">Commandes à livrées dans moins de 7 jours<i class="fa fa-circle text-warning ml-1 fs-15"></i></h4>
                        </div>
                        <div id="header-bg_collapseThree" class="collapse accordion__body" data-parent="#accordion-seven">
                            <div class="table-responsive">
								<table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
									<thead>
										<tr>
											<th>reference</th>
											<th>Modele</th>
											<th>Client</th>
											<th>Tailleur</th>
											<th>etat</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php
							                while($donnees_cmd=$req_select_dash3->fetch(PDO::FETCH_ASSOC))
							                {
							                $id=$donnees_cmd['idCommande'];
							                $reference=$donnees_cmd['reference'];
							                $modele=$donnees_cmd['modele'];
							                $modele2 = json_decode($modele);
							                $client=$donnees_cmd['client'];
							                $tailleur=$donnees_cmd['tailleur'];

							                $etat=$donnees_cmd['etat'];
							                $btn="";
							                $link="";
							                $newetat="";

							                if ($etat=="enregistrée") {
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Conception</button>';
							                	$btn="warning";
							                	$input="";
							                }elseif ($etat=="en Conception") {
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Terminer</button>';
							                	$btn="primary";
							                	$input="";
							                }if ($etat=="terminée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>valider</button>';
							                	$input="";
							                	$newetat="prête à être livrée";
							                }elseif ($etat=="prête à être livrée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Livrer</button>';
							                	$input="";
							                	$newetat="livrée";
							                }elseif ($etat=="livrée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Cloturer</button>';
							                	$input='
							                	<div class="form-group">
							                		<center><label>Reste:</label></center>
							                		<input type="text" disabled="true" class="form-control" style="color:green;" value="'.$reste.' Fcfa"  />
							                	</div>
							                		';
							                	$newetat="close";
							                }			   	               

							                $req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
							                $req_modele->execute(array('ref'=>$modele));
							                $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
							                $titre=$donnees_mod['titre'];
							                $referenceModele=$donnees_mod['reference'];
							                $photo=$donnees_mod['photo'];

							                $req_cli=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
							                $req_cli->execute(array('mat'=>$client));
							                $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
							                $prenomCli=$donnees_cli['prenom'];
							                $nomCli=$donnees_cli['nom'];
							                $matClient=$donnees_cli['matricule'];

							                $req_tail=$bdd->prepare('SELECT * FROM admin where matricule=:mat');
							                $req_tail->execute(array('mat'=>$tailleur));
							                $donnees_tail=$req_tail->fetch(PDO::FETCH_ASSOC);
							                $prenomTail=$donnees_tail['prenom'];
							                $nomTail=$donnees_tail['nom'];
							                $matTailleur=$donnees_tail['matricule'];
							            ?>
										<tr>
											<td><a href="?p=commande&id_=<?=$reference?>">#<?=$reference?></a></td>
											<td>
												<?php
												if (is_array($modele2) || is_object($modele2)){
									 
													foreach ($modele2 as $mod) {
													$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
									                $req_modele->execute(array('ref'=>$mod));
									                $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
									                $titre=$donnees_mod['titre'];
									                $referenceModele=$donnees_mod['reference'];
									                $photo=$donnees_mod['photo'];
													?>
												<a href="?p=modele&id_=<?=$mod?>"><img src="images/modeles/<?=$photo?>" width="100px" alt="<?=$titre?>"></a>
												<?php
													}
												}
												?>
											</td>
											<td><a href="?p=client&id_=<?=$matClient?>"><?=$prenomCli?> <?=$nomCli?></a></td>
											<td><a href="?p=profit&id_=<?=$matTailleur?>"><?=$prenomTail?> <?=$nomTail?></a></td>												
										</tr>
										<?php
											}
										?>
										
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                    <div class="accordion__item">
                        <?php
				            $date=date('Y-m-d');
                        	$req_select_dash4=$bdd->prepare('SELECT * FROM commandes WHERE (etat="en Conception" OR etat="enregistrée") AND DATEDIFF( dateLivraison,:date2 )<=0');
				            $req_select_dash4->execute(array('date2'=>$date));
				            $c4=$req_select_dash4->rowCount();
				        ?>
                        <div class="d-flex order-manage p-3 align-items-center mb-4" data-toggle="collapse" data-target="#header-bg_collapseFour">
							<div href="" class="btn fs-22 py-1 btn-danger px-4 mr-3"><?=$c4?></div>
							<h4 class="mb-0">Commandes en retard<i class="fa fa-circle text-danger ml-1 fs-15"></i></h4>
                        </div>
                        <div id="header-bg_collapseFour" class="collapse accordion__body" data-parent="#accordion-seven">
                            <div class="table-responsive">
								<table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
									<thead>
										<tr>
											<th>reference</th>
											<th>Modele</th>
											<th>Client</th>
											<th>Tailleur</th>
											<th>etat</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php
							                while($donnees_cmd=$req_select_dash4->fetch(PDO::FETCH_ASSOC))
							                {
							                $id=$donnees_cmd['idCommande'];
							                $reference=$donnees_cmd['reference'];
							                $modele=$donnees_cmd['modele'];
							                $modele2 = json_decode($modele);
							                $client=$donnees_cmd['client'];
							                $tailleur=$donnees_cmd['tailleur'];

							                $etat=$donnees_cmd['etat'];
							                $btn="";
							                $link="";
							                $newetat="";

							                if ($etat=="enregistrée") {
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Conception</button>';
							                	$btn="warning";
							                	$input="";
							                }elseif ($etat=="en Conception") {
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Terminer</button>';
							                	$btn="primary";
							                	$input="";
							                }if ($etat=="terminée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>valider</button>';
							                	$input="";
							                	$newetat="prête à être livrée";
							                }elseif ($etat=="prête à être livrée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Livrer</button>';
							                	$input="";
							                	$newetat="livrée";
							                }elseif ($etat=="livrée") {
							                	$btn="success";
							                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Cloturer</button>';
							                	$input='
							                	<div class="form-group">
							                		<center><label>Reste:</label></center>
							                		<input type="text" disabled="true" class="form-control" style="color:green;" value="'.$reste.' Fcfa"  />
							                	</div>
							                		';
							                	$newetat="close";
							                }			   

							                $req_cli=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
							                $req_cli->execute(array('mat'=>$client));
							                $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
							                $prenomCli=$donnees_cli['prenom'];
							                $nomCli=$donnees_cli['nom'];
							                $matClient=$donnees_cli['matricule'];

							                $req_tail=$bdd->prepare('SELECT * FROM admin where matricule=:mat');
							                $req_tail->execute(array('mat'=>$tailleur));
							                $donnees_tail=$req_tail->fetch(PDO::FETCH_ASSOC);
							                $prenomTail=$donnees_tail['prenom'];
							                $nomTail=$donnees_tail['nom'];
							                $matTailleur=$donnees_tail['matricule'];
							            ?>
										<tr>
											<td><a href="?p=commande&id_=<?=$reference?>">#<?=$reference?></a></td>
											<td>
												<?php
												if (is_array($modele2) || is_object($modele2)){
									 
													foreach ($modele2 as $mod) {
													$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
									                $req_modele->execute(array('ref'=>$mod));
									                $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
									                $titre=$donnees_mod['titre'];
									                $referenceModele=$donnees_mod['reference'];
									                $photo=$donnees_mod['photo'];
													?>
												<a href="?p=modele&id_=<?=$mod?>"><img src="images/modeles/<?=$photo?>" width="100px" alt="<?=$titre?>"></a>
												<?php
													}
												}
												?>
											</td>
											<td><a href="?p=client&id_=<?=$matClient?>"><?=$prenomCli?> <?=$nomCli?></a></td>
											<td><a href="?p=profit&id_=<?=$matTailleur?>"><?=$prenomTail?> <?=$nomTail?></a></td>	
											<td><span class="btn btn-sm light btn-<?=$btn?> fs-16"><?=$etat?></span></td>
											<td>
												<div class="dropdown ml-auto text-right">
													<div class="btn-link" data-toggle="dropdown">
														<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
													</div>
													<div class="dropdown-menu dropdown-menu-right">
														<form method="POST" action="">
															<input type="hidden" name="reference" value="<?=$reference?>">
															<input type="hidden" name="dat1" value="<?=$dateConception?>">
															<input type="hidden" name="datLiv" value="<?=$dateLivraison?>">
															<input type="hidden" name="dat2" value="<?=$dateEnd?>">
															<input type="hidden" name="etat" value="<?=$newetat?>">
															<?=$input?>
															<?=$link?>
														</form>
														<a class="dropdown-item" href="?p=edit_commande&ref=<?=$reference ?>"><i class="fa fa-pencil scale5 text-warning mr-2"></i> Modifier</a>
														<a class="dropdown-item" href="#" data-toggle="modal" data-target="#basicModal<?=$reference?>"><i class="las la-times-circle scale5 text-danger mr-2"></i> Supprimer</a>
													</div>
												</div>
											</td>												
										</tr>
										<?php
											}
										?>
										
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="col-xl-12 col-xxl-12 col-lg-12 col-md-12">
		<div class="card">
			<div class="card-header border-0 pb-0 d-sm-flex d-block">
				<div>
					<h4 class="card-title mb-1">Résumé des commandes</h4>
				</div>
			</div>
			<div class="card-body orders-summary">
				<?php
		            $req_select=$bdd->prepare('SELECT * FROM commandes WHERE type="commande"');
		            $req_select->execute(array());
		            $c=$req_select->rowCount();
		        ?>
				<div class="d-flex order-manage p-3 align-items-center mb-4">
					<a href="javascript:void(0);" class="btn fs-22 py-1 btn-success px-4 mr-3"><?=$c?></a>
					<h4 class="mb-0">Totales Commandes<i class="fa fa-circle text-success ml-1 fs-15"></i></h4>
					<a href="?p=list_commande" class="ml-auto text-primary font-w500">Gérer les commandes<i class="ti-angle-right ml-1"></i></a>
				</div>
				<div class="row">
					<div class="col-sm-4 mb-4">
						<div class="border px-3 py-3 rounded-xl">
							<?php
					            $req_select=$bdd->prepare('SELECT * FROM commandes WHERE type="commande" AND etat="enregistrée"');
					            $req_select->execute(array());
					            $c1=$req_select->rowCount();
					        ?>
							<h2 class="fs-32 font-w600 counter"><?=$c1?></h2>
							<p class="fs-16 mb-0">Enregistrée</p>
						</div>
					</div>
					<div class="col-sm-4 mb-4">
						<div class="border px-3 py-3 rounded-xl">
							<?php
					            $req_select=$bdd->prepare('SELECT * FROM commandes WHERE type="commande" AND etat="en Conception"');
					            $req_select->execute(array());
					            $c2=$req_select->rowCount();
					        ?>
							<h2 class="fs-32 font-w600 counter"><?=$c2?></h2>
							<p class="fs-16 mb-0">En Conception</p>
						</div>
					</div>
					<div class="col-sm-4 mb-4">
						<div class="border px-3 py-3 rounded-xl">
							<?php
					            $req_select=$bdd->prepare('SELECT * FROM commandes WHERE type="commande" AND etat="prêt à être livrée"');
					            $req_select->execute(array());
					            $c3=$req_select->rowCount();
					        ?>
							<h2 class="fs-32 font-w600 counter"><?=$c3?></h2>
							<p class="fs-16 mb-0">Prêt à être livrée</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-12 col-xxl-12 col-lg-12 col-md-12">
		<div class="card">
			<div class="card-header border-0 pb-0 d-sm-flex d-block">
				<div>
					<h4 class="card-title mb-1">Résumé des retouches</h4>
				</div>
			</div>
			<div class="card-body orders-summary">
				<div class="d-flex order-manage p-3 align-items-center mb-4">
					<?php
			            $req_select=$bdd->prepare('SELECT * FROM commandes WHERE type="retouche"');
			            $req_select->execute(array());
			            $r=$req_select->rowCount();
			        ?>
					<a href="javascript:void(0);" class="btn fs-22 py-1 btn-primary px-4 mr-3"><?=$r?></a>
					<h4 class="mb-0">Totales retouches <i class="fa fa-circle text-primary ml-1 fs-15"></i></h4>
					<a href="?p=list_retouche" class="ml-auto text-success font-w500">Gérer les retouches <i class="ti-angle-right ml-1"></i></a>
				</div>
				<div class="row">
					<div class="col-sm-4 mb-4">
						<div class="border px-3 py-3 rounded-xl">
							<?php
					            $req_select=$bdd->prepare('SELECT * FROM commandes WHERE type="retouche" AND etat="enregistrée"');
					            $req_select->execute(array());
					            $r1=$req_select->rowCount();
					        ?>
							<h2 class="fs-32 font-w600 counter"><?=$r1?></h2>
							<p class="fs-16 mb-0">Enregistrée</p>
						</div>
					</div>
					<div class="col-sm-4 mb-4">
						<div class="border px-3 py-3 rounded-xl">
							<?php
					            $req_select=$bdd->prepare('SELECT * FROM commandes WHERE type="retouche" AND etat="en Conception"');
					            $req_select->execute(array());
					            $r2=$req_select->rowCount();
					        ?>
							<h2 class="fs-32 font-w600 counter"><?=$r2?></h2>
							<p class="fs-16 mb-0">En cours</p>
						</div>
					</div>
					<div class="col-sm-4 mb-4">
						<div class="border px-3 py-3 rounded-xl">
							<?php
					            $req_select=$bdd->prepare('SELECT * FROM commandes WHERE type="retouche" AND etat="livrée"');
					            $req_select->execute(array());
					            $r3=$req_select->rowCount();
					        ?>
							<h2 class="fs-32 font-w600 counter"><?=$r3?></h2>
							<p class="fs-16 mb-0">Livrée</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-3 col-lg-6 col-sm-6">
		<div class="widget-stat card bg-primary">
			<div class="card-body  p-4">
				<div class="media">
					<span class="mr-3">
						<i class="flaticon-381-diamond"></i>
					</span>
					<div class="media-body text-white text-right">
						<p class="mb-1">Collection</p>
						<?php
				            $req_select=$bdd->prepare('SELECT * FROM collection');
				            $req_select->execute(array());
				            $n=$req_select->rowCount();
				        ?>
						<h3 class="text-white"><?=$n?></h3>
					</div>
				</div>
			</div>
		</div>
    </div>
    <div class="col-xl-3 col-lg-6 col-sm-6">
		<div class="widget-stat card bg-warning">
			<div class="card-body  p-4">
				<div class="media">
					<span class="mr-3">
						<i class="flaticon-381-diamond"></i>
					</span>
					<div class="media-body text-white text-right">
						<p class="mb-1">Modeles</p>
						<?php
				            $req_select=$bdd->prepare('SELECT * FROM modeles');
				            $req_select->execute(array());
				            $n=$req_select->rowCount();
				        ?>
						<h3 class="text-white"><?=$n?></h3>
					</div>
				</div>
			</div>
		</div>
    </div>
	<div class="col-xl-3 col-lg-6 col-sm-6">
		<div class="widget-stat card bg-danger">
			<div class="card-body  p-4">
				<div class="media">
					<span class="mr-3">
						<i class="flaticon-381-layer"></i>
					</span>
					<div class="media-body text-white text-right">
						<p class="mb-1">Commandes</p>
						<?php
				            $req_select=$bdd->prepare('SELECT * FROM commandes WHERE type="commande"');
				            $req_select->execute(array());
				            $n=$req_select->rowCount();
				        ?>
						<h3 class="text-white"><?=$n?></h3>
					</div>
				</div>
			</div>
		</div>
    </div>
	<div class="col-xl-3 col-lg-6 col-sm-6">
		<div class="widget-stat card bg-success">
			<div class="card-body p-4">
				<div class="media">
					<span class="mr-3">
						<i class="flaticon-381-layer"></i>
					</span>
					<div class="media-body text-white text-right">
						<p class="mb-1">Retouches</p>
						<?php
				            $req_select=$bdd->prepare('SELECT * FROM commandes WHERE type="retouche"');
				            $req_select->execute(array());
				            $n=$req_select->rowCount();
				        ?>
						<h3 class="text-white"><?=$n?></h3>
					</div>
				</div>
			</div>
		</div>
    </div>
	<div class="col-xl-3 col-lg-6 col-sm-6">
		<div class="widget-stat card bg-info">
			<div class="card-body p-4">
				<div class="media">
					<span class="mr-3">
						<i class="flaticon-381-user-7"></i>
					</span>
					<div class="media-body text-white text-right">
						<p class="mb-1">Tailleurs</p>
						<?php
				            $req_select=$bdd->prepare('SELECT * FROM admin WHERE role="tailleur"');
				            $req_select->execute(array());
				            $n=$req_select->rowCount();
				        ?>
						<h3 class="text-white"><?=$n?></h3>
					</div>
				</div>
			</div>
		</div>
    </div>
	<div class="col-xl-3 col-lg-6 col-sm-6">
		<div class="widget-stat card bg-primary">
			<div class="card-body p-4">
				<div class="media">
					<span class="mr-3">
						<i class="la la-user"></i>
					</span>
					<div class="media-body text-white text-right">
						<p class="mb-1">Clients</p>
						<?php
				            $req_select=$bdd->prepare('SELECT * FROM clients');
				            $req_select->execute(array());
				            $n=$req_select->rowCount();
				        ?>
						<h3 class="text-white"><?=$n?></h3>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>