<?php
  if (isset($erreur)) {
     echo $erreur;
  }

  ?>
  <!-- row -->
  
	<?php include('carousel.php'); ?>
	<!--<div class="alert alert-warning alert-dismissible fade show">
		<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
		<strong>Attention!</strong> la commande #345278 est en retard de 2 jours.
		<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
	    </button>
	</div>-->
  <div class="row">
	<div class="col-12">
		<div class="table-responsive">
			<table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
				<thead>
					<tr>
						<th>reference</th>
						<th>Modele</th>
						<th>Type</th>
						<th>Client</th>
						<th>Tailleur</th>
						<th>date enregistré</th>
						<th>date de livraison</th>
						<th>etat</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
              $req_cmd=$bdd->prepare('SELECT * FROM commandes WHERE etat="enregistrée" OR etat="en Conception" OR etat="terminée" ORDER BY idCommande DESC');
              $req_cmd->execute(array());
              while($donnees_cmd=$req_cmd->fetch(PDO::FETCH_ASSOC))
              {
             	$id=$donnees_cmd['idCommande'];
              $reference=$donnees_cmd['reference'];
              $modele=$donnees_cmd['modele'];
              $type=$donnees_cmd['type'];
              $modele2=json_decode($donnees_cmd['modele']);
              $client=$donnees_cmd['client'];
              $tailleur=$donnees_cmd['tailleur'];
              $dateCommande=$donnees_cmd['dateCommande'];
              $dateConception=$donnees_cmd['dateConception'];
              $dateEnd=$donnees_cmd['dateEnd'];
              $dateLivraison=$donnees_cmd['dateLivraison'];
              $etat=$donnees_cmd['etat'];	
              $btn="";
              $link="";
              $newetat="";
              if ($etat=="enregistrée") {
              	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Conception</button>';
              	//$link="Conception";
              	$btn="warning";
              	$newetat="en Conception";
              }elseif ($etat=="en Conception") {
              	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Terminer</button>';
              	//$link="Terminer";
              	$btn="primary";
              	$newetat="terminée";
              }elseif (($etat=="terminée") OR ($etat=="terminée")) {
              	$btn="success";
              }

              $req_cli=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
              $req_cli->execute(array('mat'=>$client));
              $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
              $prenomCli=$donnees_cli['prenom'];
              $nomCli=$donnees_cli['nom'];
              $matClient=$donnees_cli['matricule'];

          ?>
					<tr>
						<td><a href="?p=commande&id_=<?=$reference?>">#<?=$reference?></a></td>
						<td>
							<?php
								foreach ($modele2 as $mod) {
								$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
				                $req_modele->execute(array('ref'=>$mod));
				                $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
				                $titre=$donnees_mod['titre'];
				                $referenceModele=$donnees_mod['reference'];
				                $photo=$donnees_mod['photo'];
								?>
							<a href="?p=modele&id_=<?=$mod?>"><img src="images/modeles/<?=$photo?>" width="100px" alt="<?=$referenceModele?>"></a>
							<?php
							}
							?>
						</td>
						<td><?=$type?></td>
						<td><a href="?p=client&id_=<?=$matClient?>"><?=$prenomCli?> <?=$nomCli?></a></td>
						<td>
							<?php
							if ($tailleur==0) {
								?>
								<form method="POST" action="">
										<div class="form-group">
				              <select name="tailor">
				              	<?php
                            $req_modele=$bdd->prepare('SELECT * FROM tailleur WHERE activation=1');
                            $req_modele->execute(array());
                            while($donnees=$req_modele->fetch(PDO::FETCH_ASSOC))
                            {
                            $id=$donnees['idTailleur'];
                            $prenom=$donnees['prenom'];
                            $nom=$donnees['nom'];;
                            $matricule=$donnees['matricule'];
                        ?>
				              	<option value="<?=$matricule?>"><?=$prenom?> <?=$nom?></option>
				              	<?php } ?>
				              </select>
				            </div>
				            <div class="form-group">
											<input type="hidden" name="reference" value="<?=$reference?>">
											<button class="btn btn-primary" name="assign">Assigner</button>
				            </div>
								</form>
								<?php
							}else{
								$req_tailor=$bdd->prepare('SELECT * FROM tailleur where matricule=:mat');
	              $req_tailor->execute(array('mat'=>$tailleur));
	              $donnees_tailor=$req_tailor->fetch(PDO::FETCH_ASSOC);
	              $prenomTail=$donnees_tailor['prenom'];
	              $nomTail=$donnees_tailor['nom'];
	              ?>
	              <a href="?p=profit&id_=<?=$tailleur?>"><?=$prenomTail?> <?=$nomTail?></a>
	              <?php

							}
							?>
						</td>
						<td><?=$dateCommande?></td>
						<td><?=$dateLivraison?></td>
						<td><span class="btn btn-sm light btn-<?=$btn?> fs-16"><?=$etat?></span></td>
						<td>
							<div class="dropdown ml-auto text-right">
								<div class="btn-link" data-toggle="dropdown">
									<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
								</div>
								<div class="dropdown-menu dropdown-menu-right">
									
									<form method="POST" action="">
										<input type="hidden" name="reference" value="<?=$reference?>">
										<input type="hidden" name="dat1" value="<?=$dateConception?>">
										<input type="hidden" name="dat2" value="<?=$dateEnd?>">
										<input type="hidden" name="datLiv" value="<?=$dateLivraison?>">
										<input type="hidden" name="etat" value="<?=$newetat?>">
										<?=$link?>
									</form>
								</div>
							</div>
						</td>												
					</tr>
					<?php
						}
					?>
					
				</tbody>
			</table>
		</div>
      </div>
</div>