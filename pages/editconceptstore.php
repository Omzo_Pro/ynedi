<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Ajout ConceptStore!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">ConceptStore</a></li>
                </ol>
            </div>
        </div>
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }
        if(isset($_GET['id_'])){
                $matricule=$_GET['id_'];
        ?>
        <!-- row -->
        <div class="row">
            
        <?php include('includes/carousel.php'); ?>
            <div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Formulaire pour ajouter un conceptstore</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <?php 
                                $req_concepstore=$bdd->prepare('SELECT * FROM conceptstores WHERE matricule=:mat');
                                $req_concepstore->execute(array('mat'=>$matricule));
                                $donnees=$req_concepstore->fetch(PDO::FETCH_ASSOC);
                                    $id=$donnees['id'];
                                    $nom=$donnees['nom'];;
                                    $adresse=$donnees['adresse'];
                                    $photo=$donnees['logo'];
                                    $contact=$donnees['contact'];
                                    $matConcept=$donnees['matricule'];
                            ?>
                            <form method="POST" action="" enctype="multipart/form-data">

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label>Nom</label>
                                        <input type="text" name="nom" style="color:green" value="<?=$nom?>" class="form-control" placeholder="Nom">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Adresse</label>
                                    <textarea class="form-control" name="adresse" style="color:green"><?=$adresse?></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Contact</label>
                                    <textarea class="form-control" name="contact" style="color:green"><?=$contact?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Logo</label>
                                    <input type="file" name="logo" class="form-control">
                                    <input type="hidden"  name="logo" value="<?=$photo ?>">
                                    <label style="color:green"><?=$photo ?><label>
                                </div>
                                <input type="hidden" name="matricule" value="<?=$matricule?>">
                                <button type="submit" name="edit_conceptstore" class="form-control btn btn-primary">Valider</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>