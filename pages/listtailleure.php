<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Liste des Tailleurs!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Tailleurs</a></li>
                </ol>
            </div>
        </div>
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm mb-0 table-striped">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Email</th>
                                        <th>Téléphone</th>
                                        <th class=" pl-5" style="min-width: 200px;">Addresse
                                        </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="customers">
                                    <?php
                                        $req_modele=$bdd->prepare('SELECT * FROM tailleur WHERE activation=1');
                                        $req_modele->execute(array());
                                        while($donnees=$req_modele->fetch(PDO::FETCH_ASSOC))
                                        {
                                        $id=$donnees['idTailleur'];
                                        $prenom=$donnees['prenom'];
                                        $nom=$donnees['nom'];;
                                        $email=$donnees['email'];
                                        $adresse=$donnees['adresse'];
                                        $photo=$donnees['profile'];
                                        $telephone=$donnees['telephone'];
                                        $matTailleur=$donnees['matricule'];
                                    ?>
                                    <tr class="btn-reveal-trigger">
                                        <td class="py-3">
                                            <a href="?p=profit&id_=<?=$matTailleur?>">
                                                <div class="media d-flex align-items-center">
                                                    <div class="avatar avatar-xl mr-2">
                                                        <div class=""><img class="rounded-circle img-fluid"
                                                                src="images/profile/<?=$photo?>" width="30" alt="<?=$prenom?>" />
                                                        </div>
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="mb-0 fs--1"><?=$prenom?> <?=$nom?></h5>
                                                    </div>
                                                </div>
                                            </a>
                                        </td>
                                        <td class="py-2"><a
                                                href="mailto:<?=$email?>"><?=$email?></a></td>
                                        <td class="py-2"> <a href="tel:<?=$telephone?>"><?=$telephone?></a></td>
                                        <td class="py-2 pl-5"><?=$adresse?></td>
                                        <td class="py-2 text-right">
                                            <div class="dropdown"><button class="btn btn-primary tp-btn-light sharp" type="button" data-toggle="dropdown"><span class="fs--1"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg></span></button>
                                                <div class="dropdown-menu dropdown-menu-right border py-0">
                                                    <div class="py-2">
                                                    <a class="dropdown-item"  href="?p=edit_tailleur&id_=<?=$matTailleur ?>">Modifier</a><a class="dropdown-item text-danger" href="#!" data-toggle="modal" data-target="#basicModal<?=$matTailleur?>">Supprimer</a>
                                                </div>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="basicModal<?=$matTailleur?>">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Suppression</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">Voulez-vous vraiment supprimer &hellip; ?</div>
                                                <form method="POST" action="">
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger light" data-dismiss="modal">Annuler</button>
                                                        <input type="hidden" name="matricule" value="<?php echo $matTailleur ?>"/>
                                                        <button name="sup_tail" class="btn btn-danger">Confirmer suppression</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>