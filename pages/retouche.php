<?php
if ($connected==1) {

if(isset($_GET['id_'])){
        $reference=$_GET['id_'];

	$req_cmd=$bdd->prepare('SELECT * FROM retouches WHERE reference=:ref');
	$req_cmd->execute(array('ref'=>$reference));
	$donnees_cmd=$req_cmd->fetch(PDO::FETCH_ASSOC);
	$id=$donnees_cmd['idRetouche'];
	$reference=$donnees_cmd['reference'];
	$modele=$donnees_cmd['modele'];
	$client=$donnees_cmd['client'];
	$tailleur=$donnees_cmd['tailleur'];
	$dateRetouche=$donnees_cmd['dateRetouche'];
	$dateConception=$donnees_cmd['dateConception'];
	$dateEnd=$donnees_cmd['dateEnd'];
	$delaiLivraison=$donnees_cmd['delaiLivraison'];
	$etat=$donnees_cmd['etat'];
	$dateLivraison=$dateRetouche+$delaiLivraison;
	$btn="";
	if ($etat=="enregistré") {
		$btn="warning";
	}elseif ($etat=="en Conception") {
		$btn="primary";
	}elseif ($etat=="livré") {
		$btn="success";
	}

	$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
	$req_modele->execute(array('ref'=>$modele));
	$donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
	$titre=$donnees_mod['titre'];
	$referenceModele=$donnees_mod['reference'];
	$photo=$donnees_mod['photo'];

	$req_cli=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
	$req_cli->execute(array('mat'=>$client));
	$donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
	$prenomCli=$donnees_cli['prenom'];
	$nomCli=$donnees_cli['nom'];
	$adresseCli=$donnees_cli['adresse'];
	$telephoneCli=$donnees_cli['telephone'];
	$matClient=$donnees_cli['matricule'];

	$req_tail=$bdd->prepare('SELECT * FROM admin where matricule=:mat');
	$req_tail->execute(array('mat'=>$tailleur));
	$donnees_tail=$req_tail->fetch(PDO::FETCH_ASSOC);
	$prenomTail=$donnees_tail['prenom'];
	$nomTail=$donnees_tail['nom'];
	$profile=$donnees_tail['profile'];
	$matTailleur=$donnees_tail['matricule'];


?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <!-- row -->
	<div class="container-fluid">
		<div class="form-head d-flex mb-3 align-items-start">
			<div class="mr-auto d-none d-lg-block">
				<h2 class="text-black font-w600 mb-0">Retouche ID #<?=$reference?></h2>
				<ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil" class="text-primary">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Detaille Retouche</a></li>
                </ol>
			</div>
			<div class="dropdown mb-md-3 mb-2 ml-auto">
				<div class="btn btn-<?=$btn?>">
					<svg width="22" class="mr-2" height="28" viewBox="0 0 22 28" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.16647 27.9558C9.25682 27.9856 9.34946 28.0001 9.44106 28.0001C9.71269 28.0001 9.97541 27.8732 10.1437 27.6467L21.5954 12.2248C21.7926 11.9594 21.8232 11.6055 21.6746 11.31C21.526 11.0146 21.2236 10.8282 20.893 10.8282H13.1053V0.874999C13.1053 0.495358 12.8606 0.15903 12.4993 0.042327C12.1381 -0.0743215 11.7428 0.0551786 11.5207 0.363124L0.397278 15.7849C0.205106 16.0514 0.178364 16.403 0.327989 16.6954C0.477614 16.9878 0.77845 17.1718 1.10696 17.1718H8.56622V27.125C8.56622 27.5024 8.80816 27.8373 9.16647 27.9558ZM2.81693 15.4218L11.3553 3.58389V11.7032C11.3553 12.1865 11.7471 12.5782 12.2303 12.5782H19.1533L10.3162 24.479V16.2968C10.3162 15.8136 9.92444 15.4218 9.44122 15.4218H2.81693Z" fill="#fff"/></svg>
					<span><?=$etat?></span>
				</div>
				<a href="?p=invoice&ref_=<?= $reference ?>">Facture</a>
			</div>
		</div>
        <div class="row">
			<div class="col-xl-3 col-xxl-3 col-lg-12 col-md-12">
				<div class="row">
					<div class="col-xl-12 col-lg-6 ">
						<div class="card h-auto">
							<div class="card-body text-center">
								<img src="images/profile/<?=$profile?>" width="150" class="rounded-circle mb-4" alt=""/>
								<h4 class="mb-3 text-black font-w600"><?=$prenomTail?> <?=$nomTail?></h4>
								<a href="" class="btn btn-primary light btn-xs">Tailleur</a>
							</div>
							<div class="card bg-secondary sticky mb-0">
								<div class="card-header border-0 pb-0">
									<h5 class="card-title text-white mt-2">Modele</h5>
								</div>
								<div class="card-body pt-3">
									<img src="images/modeles/<?=$photo?>" width="150" class="mb-4" alt="<?=$titre?>"/>
									<p class="fs-14 op7 text-white"><?=$titre?> </p>
								</div>
								<div class="card-footer border-0 py-4 bg-warning rounded-xl">
									<h5 class="card-title text-white mt-2">Données de la cliente</h5>
									<div class="media align-items-center">
										<img class="mr-3 img-fluid rounded-circle" width="50" src="./images/delivery.png" alt="DexignZone">
										<div class="media-body">
											<h5 class="my-0 text-white"><?=$prenomCli?> <?=$nomCli?></h5>
											<h5 class="my-0 text-white"><?=$telephoneCli?></h5>
											<h5 class="my-0 text-white"><?=$adresseCli?></h5>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 col-lg-6">
						<div class="card">
							<div class="card-header border-0 pb-0">
								<h4 class="card-title">Historique</h4>
							</div>
							<div class="card-body">
								<div class="widget-timeline-icon">
									<ul class="timeline">
										<li>
											<div class="icon bg-primary"></div>
											<a class="timeline-panel text-muted" href="#">
												<h4 class="mb-2">Terminé</h4>
												<p class="fs-15 mb-0 "><?=$dateEnd?></p>
											</a>
										</li>
										<li>
											<div class="icon bg-primary"></div>
											<a class="timeline-panel text-muted" href="#">
												<h4 class="mb-2">Conception</h4>
												<p class="fs-15 mb-0 "><?=$dateConception?></p>
											</a>
										</li>
										<li>
											<div class="icon bg-primary"></div>
											<a class="timeline-panel text-muted" href="#">
												<h4 class="mb-2">Enregistrement</h4>
												<p class="fs-15 mb-0 "><?=$dateRetouche?></p>
											</a>
										</li>
									</ul>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		 </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php 
	}
}
else{
	include 'pages/login.php';
}
?>