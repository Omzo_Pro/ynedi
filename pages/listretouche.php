<?php
	if ($connected==1) {
?>
<div class="content-body">
    <!-- row -->
	<div class="container-fluid">
		<div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Liste des retouches!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Retouche</a></li>
                </ol>
            </div>
        </div>
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }

        ?>
        <!-- row -->
        <div class="row">
			<div class="col-12">
				<div class="table-responsive">
					<table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;">
						<thead>
							<tr>
								<th>reference</th>
								<th>Modele</th>
								<th>Client</th>
								<th>Tailleur</th>
								<th>date enregistré</th>
								<th>date de livraison</th>
								<th>etat</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
				                $req_cmd=$bdd->prepare('SELECT * FROM commandes WHERE type="retouche" AND etat!="close" ORDER BY idCommande DESC');
				                $req_cmd->execute(array());
				                while($donnees_cmd=$req_cmd->fetch(PDO::FETCH_ASSOC))
				                {
				                $id=$donnees_cmd['idCommande'];
				                $reference=$donnees_cmd['reference'];
				                $modele=$donnees_cmd['modele'];
				                //$modele2=json_decode($donnees_cmd['modele']);
				                $modele2 = json_decode($modele);
				                $client=$donnees_cmd['client'];
				                $tailleur=$donnees_cmd['tailleur'];
				                $dateCommande=$donnees_cmd['dateCommande'];
				                $dateConception=$donnees_cmd['dateConception'];
				                $dateEnd=$donnees_cmd['dateEnd'];
				                $dateLivraison=$donnees_cmd['dateLivraison'];
				                $etat=$donnees_cmd['etat'];
				                $reste=$donnees_cmd['reste'];
				                $btn="";
				                $link="";
				                $newetat="";

				                if ($etat=="enregistrée") {
				                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Conception</button>';
				                	$btn="warning";
				                	$input="";
				                }elseif ($etat=="en Conception") {
				                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Terminer</button>';
				                	$btn="primary";
				                	$input="";
				                }if ($etat=="terminée") {
				                	$btn="success";
				                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>valider</button>';
				                	$input="";
				                	$newetat="prête à être livrée";
				                }elseif ($etat=="prête à être livrée") {
				                	$btn="success";
				                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Livrer</button>';
				                	$input="";
				                	$newetat="livrée";
				                }elseif ($etat=="livrée") {
				                	$btn="success";
				                	$link='<button class="dropdown-item" name="changement_etat"><i class="las la-check-square scale5 text-primary mr-2"></i>Cloturer</button>';
				                	$input='
				                	<div class="form-group">
				                		<center><label>Reste:</label></center>
				                		<input type="text" disabled="true" class="form-control" style="color:green;" value="'.$reste.' Fcfa"  />
				                	</div>
				                		';
				                	$newetat="close";
				                }

				                $req_cli=$bdd->prepare('SELECT * FROM clients where matricule=:mat');
				                $req_cli->execute(array('mat'=>$client));
				                $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
				                $prenomCli=$donnees_cli['prenom'];
				                $nomCli=$donnees_cli['nom'];
				                $matClient=$donnees_cli['matricule'];

				                $req_tail=$bdd->prepare('SELECT * FROM tailleur where matricule=:mat');
				                $req_tail->execute(array('mat'=>$tailleur));
				                $donnees_tail=$req_tail->fetch(PDO::FETCH_ASSOC);
				                $prenomTail=$donnees_tail['prenom'];
				                $nomTail=$donnees_tail['nom'];
				                $matTailleur=$donnees_tail['matricule'];


				            ?>
							<tr>
								<td><a href="?p=commande&id_=<?=$reference?>">#<?=$reference?></a></td>
								<td>
									<?php
									if (is_array($modele2) || is_object($modele2))
									{
									 
									foreach ($modele2 as $mod) {
										$req_modele=$bdd->prepare('SELECT * FROM modeles where reference=:ref');
						                $req_modele->execute(array('ref'=>$mod));
						                $donnees_mod=$req_modele->fetch(PDO::FETCH_ASSOC);
						                $titre=$donnees_mod['titre'];
						                $referenceModele=$donnees_mod['reference'];
						                $photo=$donnees_mod['photo'];
										?>
									<label><?=$titre?></label><br>
									<a href="?p=modele&id_=<?=$mod?>"><img src="images/modeles/<?=$photo?>" width="100px" alt="<?=$titre?>"></a><hr>
									<?php
										}
									}
									?>
								</td>
								<td><a href="?p=client&id_=<?=$matClient?>"><?=$prenomCli?> <?=$nomCli?></a></td>
								<td>
									<?php
									if ($tailleur==0) {
											?>
									<form method="POST" action="">
										<div class="form-group">
							              <select name="tailor">
							              	<?php
				                            $req_modele=$bdd->prepare('SELECT * FROM tailleur WHERE activation=1');
				                            $req_modele->execute(array());
				                            while($donnees=$req_modele->fetch(PDO::FETCH_ASSOC))
				                            {
				                            $id=$donnees['idTailleur'];
				                            $prenom=$donnees['prenom'];
				                            $nom=$donnees['nom'];;
				                            $matricule=$donnees['matricule'];
			                        		?>
							              	<option value="<?=$matricule?>"><?=$prenom?> <?=$nom?></option>
							              	<?php } ?>
							              </select>
							            </div>
							            <div class="form-group">
											<input type="hidden" name="reference" value="<?=$reference?>">
											<button class="btn btn-primary" name="assign">Assigner</button>
							            </div>
									</form>
									<?php
									}else{
										$req_tailor=$bdd->prepare('SELECT * FROM tailleur where matricule=:mat');
							            $req_tailor->execute(array('mat'=>$tailleur));
							            $donnees_tailor=$req_tailor->fetch(PDO::FETCH_ASSOC);
							            $prenomTail=$donnees_tailor['prenom'];
							            $nomTail=$donnees_tailor['nom'];
							        ?>
						            <a href="?p=profit&id_=<?=$tailleur?>"><?=$prenomTail?> <?=$nomTail?></a>
				              		<?php
										}
									?>
								</td>
								<td><?=$dateCommande?></td>
								<td><?=$dateLivraison?></td>
								<td><span class="btn btn-sm light btn-<?=$btn?> fs-16"><?=$etat?></span></td>
								<td>
									<div class="dropdown ml-auto text-right">
										<div class="btn-link" data-toggle="dropdown">
											<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><rect x="0" y="0" width="24" height="24"></rect><circle fill="#000000" cx="5" cy="12" r="2"></circle><circle fill="#000000" cx="12" cy="12" r="2"></circle><circle fill="#000000" cx="19" cy="12" r="2"></circle></g></svg>
										</div>
										<div class="dropdown-menu dropdown-menu-right">
											<form method="POST" action="">
												<input type="hidden" name="reference" value="<?=$reference?>">
												<input type="hidden" name="dat1" value="<?=$dateConception?>">
												<input type="hidden" name="datLiv" value="<?=$dateLivraison?>">
												<input type="hidden" name="dat2" value="<?=$dateEnd?>">
												<input type="hidden" name="etat" value="<?=$newetat?>">
												<?=$input?>
												<?=$link?>
											</form>
											<a class="dropdown-item" href="#"><i class="fa fa-pencil scale5 text-warning mr-2"></i> Modifier</a>
											<a class="dropdown-item" href="#" data-toggle="modal" data-target="#basicModal<?=$reference?>"><i class="las la-times-circle scale5 text-danger mr-2"></i> Supprimer</a>
										</div>
									</div>
								</td>												
							</tr>
							<div class="modal fade" id="basicModal<?=$reference?>">
							    <div class="modal-dialog" role="document">
							        <div class="modal-content">
							            <div class="modal-header">
							                <h5 class="modal-title">Suppression</h5>
							                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
							                </button>
							            </div>
							            <div class="modal-body">Voulez-vous vraiment supprimer &hellip; ?</div>
							            <form method="POST" action="">
								            <div class="modal-footer">
								            	<button type="button" class="btn btn-danger light" data-dismiss="modal">Annuler</button>
									            <input type="hidden" name="reference" value="<?php echo $reference ?>"/>
								                <button name="sup_com" class="btn btn-danger">Confirmer suppression</button>
								            </div>
							            </form>
							        </div>
							    </div>
							</div>
							<?php
								}
							?>
							
						</tbody>
					</table>
				</div>
            </div>
		</div>
    </div>
</div>
<?php
}
else{
	include 'pages/login.php';
}
?>