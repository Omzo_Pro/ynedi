<?php
    if ($connected==1) {

    if(isset($_GET['id_'])){
        $client=$_GET['id_'];

    $req_cli=$bdd->prepare('SELECT * FROM clients WHERE matricule=:mat');
    $req_cli->execute(array('mat'=>$client));
    $donnees_cli=$req_cli->fetch(PDO::FETCH_ASSOC);
    $id=$donnees_cli['idClient'];
    $prenomClient=$donnees_cli['prenom'];
    $nomClient=$donnees_cli['nom'];
    $emailClient=$donnees_cli['email'];
    $adresseClient=$donnees_cli['adresse'];
    $commentaire=$donnees_cli['commentaire'];
    $whatsapp=$donnees_cli['whatsapp'];
    $telephoneClient=$donnees_cli['telephone'];
    $jourNaiss=$donnees_cli['jourNaiss'];
    $moisNaiss=$donnees_cli['moisNaiss'];
    $verification=$donnees_cli['verification'];
    $span="";
    if ($verification==1) {
        $span='<span class="badge badge-success">
                            Verifiée<span class="ml-1 fa fa-check"></span>
                        </span>';
    }elseif ($verification==0) {
        $span='<span class="badge badge-warning">
                    Non Verifiée<span class="ml-1 fa fa-ban"></span>
                </span>';
    }

    $req_mesure=$bdd->prepare('SELECT * FROM mesures WHERE client=:mat');
    $req_mesure->execute(array('mat'=>$client));
    $donnees_mes=$req_mesure->fetch(PDO::FETCH_ASSOC);
    $epaule=$donnees_mes['epaule'];
    $poitrine=$donnees_mes['poitrine'];
    $taille=$donnees_mes['taille'];
    $ceinture=$donnees_mes['ceinture'];
    $hanche=$donnees_mes['hanche'];
    $blouse=$donnees_mes['blouse'];
    $manche34=$donnees_mes['manche34'];
    $mancheLongue=$donnees_mes['mancheLongue'];
    $mancheCourte=$donnees_mes['mancheCourte'];
    $cou=$donnees_mes['cou'];
    $tManches=$donnees_mes['tManches'];
    $poignet=$donnees_mes['poignet'];
    $cuisses=$donnees_mes['cuisses'];
    $longJupe=$donnees_mes['longJupe'];
    $jupe34=$donnees_mes['jupe34'];
    $longPantalon=$donnees_mes['longPantalon'];
    $longRobe=$donnees_mes['longRobe'];
    $longRobe34=$donnees_mes['longRobe34'];
    $longBoubou=$donnees_mes['longBoubou'];                                    
?>
 <!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4></h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Client</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }
        ?>
        <div class="row">
        	<div class="col-xl-12 col-lg-12 col-sm-12">
				<div class="card overflow-hidden">
					<div class="text-center p-3" style="background-image: url(images/profile/cover.jpg);">
						<div class="profile-photo">
							<img src="images/profile/profile.png" width="100" class="img-fluid rounded-circle" alt="">
						</div>
						<h3 class="mt-3 mb-1 text-white"><?=$prenomClient?> <?=$nomClient?></h3>
						<p class="text-white mb-0"><?=$adresseClient?></p>
						<p class="text-white mb-0"><?=$telephoneClient?></p>
                        <p class="text-white mb-0"><?=$commentaire?></p>
                        <?=$span?>
					</div>
					<ul class="list-group list-group-flush">
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Epaule</span> <strong class="text-muted"><?=$epaule?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Poitrine</span> <strong class="text-muted"><?=$poitrine?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Taille</span> <strong class="text-muted"><?=$taille?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Ceinture</span> <strong class="text-muted"><?=$ceinture?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Hanche</span> <strong class="text-muted"><?=$hanche?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Blouse</span> <strong class="text-muted"><?=$blouse?></strong></li>
                        <li class="list-group-item d-flex justify-content-between"><span class="mb-0">MancheCourte</span> <strong class="text-muted"><?=$mancheCourte?></strong></li>
                        <li class="list-group-item d-flex justify-content-between"><span class="mb-0">Manches 3/4</span> <strong class="text-muted"><?=$manche34?></strong></li>
                        <li class="list-group-item d-flex justify-content-between"><span class="mb-0">Manche Longue</span> <strong class="text-muted"><?=$mancheLongue?></strong></li>                        <li class="list-group-item d-flex justify-content-between"><span class="mb-0">Tour de manche</span> <strong class="text-muted"><?=$tManches?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Cou</span> <strong class="text-muted"><?=$cou?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Poignet</span> <strong class="text-muted"><?=$poignet?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Cuisse</span> <strong class="text-muted"><?=$cuisses?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Long Jupe</span> <strong class="text-muted"><?=$longJupe?></strong></li>
                        <li class="list-group-item d-flex justify-content-between"><span class="mb-0">Jupe 3/4</span> <strong class="text-muted"><?=$jupe34?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Long Pantalon</span> <strong class="text-muted"><?=$longPantalon?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Long Robe</span> <strong class="text-muted"><?=$longRobe?></strong></li>
                        <li class="list-group-item d-flex justify-content-between"><span class="mb-0">Long Robe 3/4</span> <strong class="text-muted"><?=$longRobe34?></strong></li>
						<li class="list-group-item d-flex justify-content-between"><span class="mb-0">Long Boubou</span> <strong class="text-muted"><?=$longBoubou?></strong></li>
					</ul>
                    <div class="card-footer border-0 mt-0">
                        <?php
                            if (($role=='admin') OR ($role=='super_admin')){
                        ?>							
						<button class="btn btn-primary btn-lg btn-block">
							<i class="fa fa-bell-o"></i>Anniversaire: <?=$jourNaiss?>/<?=$moisNaiss?>
						</button>
                        <?php
                            } 
                            if ($verification==0) {
                        ?>
                        <form method="POST" action="">
                            <input type="hidden" name="matricule" value="<?=$client?>"><br>
                            <button name="verifier" class="btn btn-success btn-lg btn-block">
                                Valider verification
                            </button>
                        </form>
                        <?php 
                            }
                        ?>
                    </div>
                </div>
			</div>
            <div class="col-xl-12 col-xxl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="widget-stat card">
                    <?php
                        $cumul=0; $i=0;
                        $req_cmd=$bdd->prepare('SELECT * FROM commandes WHERE client=:cli');
                                $req_cmd->execute(array('cli'=>$client));
                                while($donnees_cmd=$req_cmd->fetch(PDO::FETCH_ASSOC))
                                {
                                $somme=$donnees_cmd['somme'];
                                $remise=$donnees_cmd['remise'];
                                $sommeRemise=($somme*$remise)/100;
                                $montan=$somme-$sommeRemise;
                                $cumul+=$montan;
                                $i++;
                                }
                                $cumul1=(number_format($cumul,0,".","."));
                                    
                    ?>
                    <div class="card-body p-4">
                        <div class="media ai-icon">
                            <span class="mr-3 bgl-primary text-primary">
                                <i class="fa fa-money"></i> 
                               <!--  <svg width="20" height="36" viewBox="0 0 20 36" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.08 24.36C19.08 25.64 18.76 26.8667 18.12 28.04C17.48 29.1867 16.5333 30.1467 15.28 30.92C14.0533 31.6933 12.5733 32.1333 10.84 32.24V35.48H8.68V32.24C6.25333 32.0267 4.28 31.2533 2.76 29.92C1.24 28.56 0.466667 26.84 0.44 24.76H4.32C4.42667 25.88 4.84 26.8533 5.56 27.68C6.30667 28.5067 7.34667 29.0267 8.68 29.24V19.24C6.89333 18.7867 5.45333 18.32 4.36 17.84C3.26667 17.36 2.33333 16.6133 1.56 15.6C0.786667 14.5867 0.4 13.2267 0.4 11.52C0.4 9.36 1.14667 7.57333 2.64 6.16C4.16 4.74666 6.17333 3.96 8.68 3.8V0.479998H10.84V3.8C13.1067 3.98667 14.9333 4.72 16.32 6C17.7067 7.25333 18.5067 8.89333 18.72 10.92H14.84C14.7067 9.98667 14.2933 9.14667 13.6 8.4C12.9067 7.62667 11.9867 7.12 10.84 6.88V16.64C12.6 17.0933 14.0267 17.56 15.12 18.04C16.24 18.4933 17.1733 19.2267 17.92 20.24C18.6933 21.2533 19.08 22.6267 19.08 24.36ZM4.12 11.32C4.12 12.6267 4.50667 13.6267 5.28 14.32C6.05333 15.0133 7.18667 15.5867 8.68 16.04V6.8C7.29333 6.93333 6.18667 7.38667 5.36 8.16C4.53333 8.90667 4.12 9.96 4.12 11.32ZM10.84 29.28C12.28 29.12 13.4 28.6 14.2 27.72C15.0267 26.84 15.44 25.7867 15.44 24.56C15.44 23.2533 15.04 22.2533 14.24 21.56C13.44 20.84 12.3067 20.2667 10.84 19.84V29.28Z" fill="#2F4CDD"/></svg>-->
                            </span>
                            <div class="media-body">
                                <h3 class="mb-0 text-black"><span class=""><?=$cumul1?></span> Fcfa</h3>
                                <p class="mb-0">Cumul d'achat</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<?php 
    }
}
else{
    include 'pages/login.php';
}
?>