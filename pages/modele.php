<?php
    if ($connected==1) {

    if(isset($_GET['id_'])){
        $modele=$_GET['id_'];

    $req_tailleur=$bdd->prepare('SELECT * FROM modeles WHERE reference=:mat');
    $req_tailleur->execute(array('mat'=>$modele));
    $donnees=$req_tailleur->fetch(PDO::FETCH_ASSOC);
    $titre=$donnees['titre'];
    $photo=$donnees['photo'];
    $description=$donnees['description'];
    $prix=$donnees['prix'];
                                    
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4></h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Modele</a></li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-3 ">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade show active" id="first">
                                        <img class="img-fluid" src="images/modeles/<?=$photo?>" alt="">
                                    </div>
                                </div>
                                <div class="tab-slide-content new-arrival-product mb-4 mb-xl-0">
                                    <!-- Nav tabs -->
                                    <ul class="nav slide-item-list mt-3" role="tablist">
                                        <li role="presentation" class="show">
                                            <a href="#first" role="tab" data-toggle="tab">
                                                <img class="img-fluid" src="images/modeles/<?=$photo?>" alt="" width="50">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--Tab slider End-->
                            <div class="col-xl-9 col-sm-12">
                                <div class="product-detail-content">
                                    <!--Product details-->
                                    <div class="new-arrival-content pr">
                                        <h4><?=$titre?></h4>
                                        <?php
                                            if (($role=='admin') OR ($role=='super_admin')){
                                        ?>
                                        <p class="price"><?=$prix?> Fcfa</p>
                                        <?php } ?>
                                        <p>Reference : <span class="item"><?=$modele?></span> </p>
                                        <h4 class="m-b-15">Description</h4>
                                        <p class="text-content"><?=$description?></p>
                                        <span class="review-text"></span><a class="product-review" href="?p=edit_modele&id_=<?=$modele ?>"><i class="fa fa-pencil scale5 text-warning mr-2"></i>Modifier</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
    }
}
else{
    include 'pages/login.php';
}
?>