<?php
    if ($connected==1) {
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Ajout d'une tenue!</h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Conceptstore</a></li>
                </ol>
            </div>
        </div>
        <?php
        if (isset($erreur)) {
           echo $erreur;
        }

        ?>
        <!-- row -->
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Formulaire pour ajouter une tenue</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form method="POST" action="" enctype="multipart/form-data">

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Conceptstore</label>
                                        <select name="conceptstore" class="form-control">
                                            <?php
                                                $req_collection=$bdd->prepare('SELECT * FROM conceptstores');
                                                $req_collection->execute(array());
                                                while($donnees=$req_collection->fetch(PDO::FETCH_ASSOC))
                                                {
                                                $matricule=$donnees['matricule'];
                                                $nom=$donnees['nom'];
                                            ?>
                                            <option value="<?=$matricule?>"><?=$nom?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Modele</label>
                                        <select name="modele" class="form-control">
                                            <?php
                                                $req_collection=$bdd->prepare('SELECT * FROM modeles');
                                                $req_collection->execute(array());
                                                while($donnees=$req_collection->fetch(PDO::FETCH_ASSOC))
                                                {
                                                $reference=$donnees['reference'];
                                                $titre=$donnees['titre'];
                                            ?>
                                            <option value="<?=$reference?>"><?=$titre?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Quantité</label>
                                        <input type="number" name="qte" class="form-control" placeholder="Quantité" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Taille</label>
                                        <input type="text" name="taille" class="form-control" placeholder="Taille" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Prix Concepstore</label>
                                        <input type="number" name="prix" class="form-control" placeholder="prix" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Date</label>
                                        <input type="date" name="dateR" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description"></textarea>
                                </div>
                                <button type="submit" name="add_tenue" class="btn btn-primary">Valider</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
}
else{
    include 'pages/login.php';
}
?>