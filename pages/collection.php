<?php
    if ($connected==1) {

    if(isset($_GET['id_'])){
        $secret=$_GET['id_'];

    $req_collection=$bdd->prepare('SELECT * FROM collection WHERE secret=:sec');
    $req_collection->execute(array('sec'=>$secret));
    $donnees=$req_collection->fetch(PDO::FETCH_ASSOC);
    $collection=$donnees['collection'];
    $description=$donnees['description'];
                                    
?>
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Collection <?=$collection?></h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?p=accueil">Accueil</a></li>
                    <li class="breadcrumb-item active"><a href="">Modèles</a></li>
                </ol>
            </div>
        </div>
        <div class="row">
             <?php
                $req_modele=$bdd->prepare('SELECT * FROM modeles WHERE collection=:col');
                $req_modele->execute(array('col'=>$secret));
                while($donnees=$req_modele->fetch(PDO::FETCH_ASSOC))
                {
                $id=$donnees['idModele'];
                $reference=$donnees['reference'];
                $titre=$donnees['titre'];
                $prix=$donnees['prix'];
                $photo=$donnees['photo'];
                $description=$donnees['description'];
            ?>
            <div class="col-lg-12 col-xl-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row m-b-30">
                            <div class="col-md-5 col-xxl-12">
                                <div class="new-arrival-product mb-4 mb-xxl-4 mb-md-0">
                                    <div class="new-arrivals-img-contnent">
                                        <img class="img-fluid" src="images/modeles/<?=$photo?>" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-xxl-12">
                                <div class="new-arrival-content position-relative">
                                    <a href="?p=modele&id_=<?=$reference?>"><h4><?=$titre?></h4></a>
                                    <p class="price"><?=$prix?> Fcfa</p>
                                    <p>Reference: <span class="item"><?=$reference?></span> </p>
                                    <p class="text-content"><?=$description?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
            <?php } ?>
        </div>
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<?php
    }
}
else{
    include 'pages/login.php';
}
?>