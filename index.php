<?php
	include('pages/header.php');
	include('pages/menu.php');

	if(!isset($_GET['p'])){
		include('pages/accueil.php');
	}
	else{
		if($_GET['p']=='accueil'){
			include('pages/accueil.php');
		}
		else if($_GET['p']=='add_collection'){
			include('pages/addcollection.php');
		}
		else if($_GET['p']=='list_collection'){
			include('pages/listcollection.php');
		}
		else if($_GET['p']=='edit_collection'){
			include('pages/editcollection.php');
		}
		else if($_GET['p']=='add_commande'){
			include('pages/addcommande.php');
		}
		else if($_GET['p']=='list_commande'){
			include('pages/listcommande.php');
		}
		else if($_GET['p']=='edit_commande'){
			include('pages/editcommande.php');
		}
		else if($_GET['p']=='add_modele'){
			include('pages/addmodele.php');
		}
		else if($_GET['p']=='list_modele'){
			include('pages/listmodele.php');
		}
		else if($_GET['p']=='edit_modele'){
			include('pages/editmodele.php');
		}
		else if($_GET['p']=='add_retouche'){
			include('pages/addretouche.php');
		}
		else if($_GET['p']=='list_retouche'){
			include('pages/listretouche.php');
		}
		else if($_GET['p']=='edit_retouche'){
			include('pages/editretouche.php');
		}
		else if($_GET['p']=='add_tailleure'){
			include('pages/addtailleure.php');
		}
		else if($_GET['p']=='list_tailleure'){
			include('pages/listtailleure.php');
		}
		else if($_GET['p']=='edit_tailleur'){
			include('pages/edittailleur.php');
		}
		else if($_GET['p']=='profit'){
			include('pages/tailleur.php');
		}
		else if($_GET['p']=='client'){
			include('pages/client.php');
		}
		else if($_GET['p']=='collection'){
			include('pages/collection.php');
		}
		else if($_GET['p']=='modele'){
			include('pages/modele.php');
		}
		else if($_GET['p']=='retouche'){
			include('pages/retouche.php');
		}
		else if($_GET['p']=='commande'){
			include('pages/commande.php');
		}
		else if($_GET['p']=='add_admin'){
			include('pages/addadmin.php');
		}
		else if($_GET['p']=='list_admin'){
			include('pages/listadmin.php');
		}
		else if($_GET['p']=='edit_admin'){
			include('pages/editadmin.php');
		}
		else if($_GET['p']=='add_client'){
			include('pages/addclient.php');
		}
		else if($_GET['p']=='list_client'){
			include('pages/listclient.php');
		}
		else if($_GET['p']=='edit_client'){
			include('pages/editclient.php');
		}
		else if($_GET['p']=='add_conceptstore'){
			include('pages/addconceptstore.php');
		}
		else if($_GET['p']=='list_conceptstore'){
			include('pages/listconceptstore.php');
		}
		else if($_GET['p']=='edit_conceptstore'){
			include('pages/editconceptstore.php');
		}
		else if($_GET['p']=='conceptstore'){
			include('pages/conceptstore.php');
		}
		else if($_GET['p']=='add_tenue'){
			include('pages/addtenue.php');
		}
		else if($_GET['p']=='edit_tenue'){
			include('pages/edittenue.php');
		}
		else if($_GET['p']=='profile'){
			include('pages/profile.php');
		}
		else if($_GET['p']=='assign'){
			include('pages/assign.php');
		}
		else if($_GET['p']=='invoice'){
			include('pages/invoice.php');
		}
		else if($_GET['p']=='test'){
			include('pages/test.php');
		}
		
	}

	include('pages/footer.php');
?>